'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var _customCheckboxRadioSwitch = require('../styles/customCheckboxRadioSwitch');

var _customCheckboxRadioSwitch2 = _interopRequireDefault(_customCheckboxRadioSwitch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ##############################
// // // Tasks styles
// #############################

var tasksStyle = Object.assign({}, _customCheckboxRadioSwitch2.default, {
	table: {
		marginBottom: '0'
	},
	tableRow: {
		position: 'relative',
		borderBottom: '1px solid #dddddd'
	},
	tableActions: {
		border: 'none',
		padding: '12px 8px !important',
		verticalAlign: 'middle'
	},
	tableCell: Object.assign({}, _styles.defaultFont, {
		padding: '0',
		verticalAlign: 'middle',
		border: 'none',
		lineHeight: '1.42857143',
		fontSize: '14px'
	}),
	tableActionButton: {
		width: '27px',
		height: '27px'
	},
	tableActionButtonIcon: {
		width: '17px',
		height: '17px'
	},
	edit: {
		backgroundColor: 'transparent',
		color: _styles.primaryColor,
		boxShadow: 'none'
	},
	close: {
		backgroundColor: 'transparent',
		color: _styles.dangerColor,
		boxShadow: 'none'
	},
	tooltip: _styles.tooltip
});
exports.default = tasksStyle;

//# sourceMappingURL=styles.js.map