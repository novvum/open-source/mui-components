'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _defaultAvatar = require('./default-avatar.png');

var _defaultAvatar2 = _interopRequireDefault(_defaultAvatar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PictureUpload = function (_React$Component) {
	_inherits(PictureUpload, _React$Component);

	function PictureUpload(props) {
		_classCallCheck(this, PictureUpload);

		var _this = _possibleConstructorReturn(this, (PictureUpload.__proto__ || Object.getPrototypeOf(PictureUpload)).call(this, props));

		_this.state = {
			file: null,
			imagePreviewUrl: _defaultAvatar2.default
		};
		_this.handleImageChange = _this.handleImageChange.bind(_this);
		_this.handleSubmit = _this.handleSubmit.bind(_this);
		return _this;
	}

	_createClass(PictureUpload, [{
		key: 'handleImageChange',
		value: function handleImageChange(e) {
			var _this2 = this;

			e.preventDefault();
			var reader = new FileReader();
			var file = e.target.files[0];
			reader.onloadend = function () {
				_this2.setState({
					file: file,
					imagePreviewUrl: reader.result
				});
			};
			reader.readAsDataURL(file);
		}
	}, {
		key: 'handleSubmit',
		value: function handleSubmit(e) {
			e.preventDefault();
			// this.state.file is the file/image uploaded
			// in this function you can save the image (this.state.file) on form submit
			// you have to call it yourself
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			return _react2.default.createElement(
				'div',
				{ className: 'picture-container' },
				_react2.default.createElement(
					'div',
					{ className: 'picture' },
					_react2.default.createElement('img', {
						src: this.state.imagePreviewUrl,
						className: 'picture-src',
						alt: '...'
					}),
					_react2.default.createElement('input', { type: 'file', onChange: function onChange(e) {
							return _this3.handleImageChange(e);
						} })
				),
				_react2.default.createElement(
					'h6',
					{ className: 'description' },
					'Choose Picture'
				)
			);
		}
	}]);

	return PictureUpload;
}(_react2.default.Component);

exports.default = PictureUpload;

//# sourceMappingURL=PictureUpload.js.map