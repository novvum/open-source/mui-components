'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _image_placeholder = require('./image_placeholder.jpg');

var _image_placeholder2 = _interopRequireDefault(_image_placeholder);

var _placeholder = require('./placeholder.jpg');

var _placeholder2 = _interopRequireDefault(_placeholder);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// used for making the prop types of this component


// core components


var ImageUpload = function (_React$Component) {
	_inherits(ImageUpload, _React$Component);

	function ImageUpload(props) {
		_classCallCheck(this, ImageUpload);

		var _this = _possibleConstructorReturn(this, (ImageUpload.__proto__ || Object.getPrototypeOf(ImageUpload)).call(this, props));

		_this.state = {
			file: null,
			imagePreviewUrl: _this.props.avatar ? _placeholder2.default : _image_placeholder2.default
		};
		_this.handleImageChange = _this.handleImageChange.bind(_this);
		_this.handleSubmit = _this.handleSubmit.bind(_this);
		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleRemove = _this.handleRemove.bind(_this);
		return _this;
	}

	_createClass(ImageUpload, [{
		key: 'handleImageChange',
		value: function handleImageChange(e) {
			var _this2 = this;

			e.preventDefault();
			var reader = new FileReader();
			var file = e.target.files[0];
			reader.onloadend = function () {
				_this2.setState({
					file: file,
					imagePreviewUrl: reader.result
				});
			};
			reader.readAsDataURL(file);
		}
	}, {
		key: 'handleSubmit',
		value: function handleSubmit(e) {
			e.preventDefault();
			// this.state.file is the file/image uploaded
			// in this function you can save the image (this.state.file) on form submit
			// you have to call it yourself
		}
	}, {
		key: 'handleClick',
		value: function handleClick() {
			this.refs.fileInput.click();
		}
	}, {
		key: 'handleRemove',
		value: function handleRemove() {
			this.setState({
				file: null,
				imagePreviewUrl: this.props.avatar ? _placeholder2.default : _image_placeholder2.default
			});
			this.refs.fileInput.value = null;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _props = this.props,
			    avatar = _props.avatar,
			    addButtonProps = _props.addButtonProps,
			    changeButtonProps = _props.changeButtonProps,
			    removeButtonProps = _props.removeButtonProps;

			return _react2.default.createElement(
				'div',
				{ className: 'fileinput text-center' },
				_react2.default.createElement('input', { type: 'file', onChange: this.handleImageChange, ref: 'fileInput' }),
				_react2.default.createElement(
					'div',
					{ className: 'thumbnail' + (avatar ? ' img-circle' : '') },
					_react2.default.createElement('img', { src: this.state.imagePreviewUrl, alt: '...' })
				),
				_react2.default.createElement(
					'div',
					null,
					this.state.file === null ? _react2.default.createElement(
						_CustomButtons2.default,
						Object.assign({}, addButtonProps, { onClick: function onClick() {
								return _this3.handleClick();
							} }),
						avatar ? 'Add Photo' : 'Select image'
					) : _react2.default.createElement(
						'span',
						null,
						_react2.default.createElement(
							_CustomButtons2.default,
							Object.assign({}, changeButtonProps, { onClick: function onClick() {
									return _this3.handleClick();
								} }),
							'Change'
						),
						avatar ? _react2.default.createElement('br', null) : null,
						_react2.default.createElement(
							_CustomButtons2.default,
							Object.assign({}, removeButtonProps, {
								onClick: function onClick() {
									return _this3.handleRemove();
								}
							}),
							_react2.default.createElement('i', { className: 'fas fa-times' }),
							' Remove'
						)
					)
				)
			);
		}
	}]);

	return ImageUpload;
}(_react2.default.Component);

ImageUpload.propTypes = {
	avatar: _propTypes2.default.bool,
	addButtonProps: _propTypes2.default.object,
	changeButtonProps: _propTypes2.default.object,
	removeButtonProps: _propTypes2.default.object
};

exports.default = ImageUpload;

//# sourceMappingURL=ImageUpload.js.map