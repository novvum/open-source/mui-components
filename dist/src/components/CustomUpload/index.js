'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ImageUpload = require('./ImageUpload');

Object.defineProperty(exports, 'ImageUpload', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ImageUpload).default;
  }
});

var _PictureUpload = require('./PictureUpload');

Object.defineProperty(exports, 'PictureUpload', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_PictureUpload).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//# sourceMappingURL=index.js.map