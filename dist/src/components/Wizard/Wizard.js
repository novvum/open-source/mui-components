'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _Card = require('../Card');

var _Card2 = _interopRequireDefault(_Card);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// @material-ui/core components

// core components


var WizardForm = function (_React$Component) {
	_inherits(WizardForm, _React$Component);

	function WizardForm(props) {
		_classCallCheck(this, WizardForm);

		var _this = _possibleConstructorReturn(this, (WizardForm.__proto__ || Object.getPrototypeOf(WizardForm)).call(this, props));

		var width;
		if (_this.props.steps.length === 1) {
			width = '100%';
		} else {
			if (window.innerWidth < 600) {
				if (_this.props.steps.length !== 3) {
					width = '50%';
				} else {
					width = 100 / 3 + '%';
				}
			} else {
				if (_this.props.steps.length === 2) {
					width = '50%';
				} else {
					width = 100 / 3 + '%';
				}
			}
		}
		_this.state = {
			currentStep: 0,
			color: _this.props.color,
			nextButton: _this.props.steps.length > 1 ? true : false,
			previousButton: false,
			finishButton: _this.props.steps.length === 1 ? true : false,
			width: width,
			movingTabStyle: {
				transition: 'transform 0s'
			},
			allStates: {}
		};
		_this.navigationStepChange = _this.navigationStepChange.bind(_this);
		_this.refreshAnimation = _this.refreshAnimation.bind(_this);
		_this.previousButtonClick = _this.previousButtonClick.bind(_this);
		_this.previousButtonClick = _this.previousButtonClick.bind(_this);
		_this.finishButtonClick = _this.finishButtonClick.bind(_this);
		_this.updateWidth = _this.updateWidth.bind(_this);
		return _this;
	}

	_createClass(WizardForm, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.refreshAnimation(0);
			window.addEventListener('resize', this.updateWidth);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			window.removeEventListener('resize', this.updateWidth);
		}
	}, {
		key: 'updateWidth',
		value: function updateWidth() {
			this.refreshAnimation(this.state.currentStep);
		}
	}, {
		key: 'navigationStepChange',
		value: function navigationStepChange(key) {
			console.log(this.state.allStates);
			if (this.props.steps) {
				var validationState = true;
				if (key > this.state.currentStep) {
					for (var i = this.state.currentStep; i < key; i++) {
						if (this[this.props.steps[i].stepId].sendState !== undefined) {
							this.setState({
								allStates: [].concat(_toConsumableArray(this.state.allStates), [_defineProperty({}, this.props.steps[i].stepId, this[this.props.steps[i].stepId].sendState())])
							});
						}
						if (this[this.props.steps[i].stepId].isValidated !== undefined && this[this.props.steps[i].stepId].isValidated() === false) {
							validationState = false;
							break;
						}
					}
				}
				if (validationState) {
					this.setState({
						currentStep: key,
						nextButton: this.props.steps.length > key + 1 ? true : false,
						previousButton: key > 0 ? true : false,
						finishButton: this.props.steps.length === key + 1 ? true : false
					});
					this.refreshAnimation(key);
				}
			}
		}
	}, {
		key: 'nextButtonClick',
		value: function nextButtonClick() {
			if (this.props.validate && (this[this.props.steps[this.state.currentStep].stepId].isValidated !== undefined && this[this.props.steps[this.state.currentStep].stepId].isValidated() || this[this.props.steps[this.state.currentStep].stepId].isValidated === undefined) || this.props.validate === undefined) {
				if (this[this.props.steps[this.state.currentStep].stepId].sendState !== undefined) {
					this.setState({
						allStates: [].concat(_toConsumableArray(this.state.allStates), [_defineProperty({}, this.props.steps[this.state.currentStep].stepId, this[this.props.steps[this.state.currentStep].stepId].sendState())])
					});
				}
				var key = this.state.currentStep + 1;
				this.setState({
					currentStep: key,
					nextButton: this.props.steps.length > key + 1 ? true : false,
					previousButton: key > 0 ? true : false,
					finishButton: this.props.steps.length === key + 1 ? true : false
				});
				this.refreshAnimation(key);
			}
		}
	}, {
		key: 'previousButtonClick',
		value: function previousButtonClick() {
			if (this[this.props.steps[this.state.currentStep].stepId].sendState !== undefined) {
				this.setState({
					allStates: [].concat(_toConsumableArray(this.state.allStates), [_defineProperty({}, this.props.steps[this.state.currentStep].stepId, this[this.props.steps[this.state.currentStep].stepId].sendState())])
				});
			}
			var key = this.state.currentStep - 1;
			if (key >= 0) {
				this.setState({
					currentStep: key,
					nextButton: this.props.steps.length > key + 1 ? true : false,
					previousButton: key > 0 ? true : false,
					finishButton: this.props.steps.length === key + 1 ? true : false
				});
				this.refreshAnimation(key);
			}
		}
	}, {
		key: 'finishButtonClick',
		value: function finishButtonClick() {
			if (this.props.validate && (this[this.props.steps[this.state.currentStep].stepId].isValidated !== undefined && this[this.props.steps[this.state.currentStep].stepId].isValidated() || this[this.props.steps[this.state.currentStep].stepId].isValidated === undefined) && this.props.finishButtonClick !== undefined) {
				this.props.finishButtonClick();
			}
		}
	}, {
		key: 'refreshAnimation',
		value: function refreshAnimation(index) {
			var total = this.props.steps.length;
			var li_width = 100 / total;
			var total_steps = this.props.steps.length;
			var move_distance = this.refs.wizard.children[0].offsetWidth / total_steps;
			var index_temp = index;
			var vertical_level = 0;

			var mobile_device = window.innerWidth < 600 && total > 3;

			if (mobile_device) {
				move_distance = this.refs.wizard.children[0].offsetWidth / 2;
				index_temp = index % 2;
				li_width = 50;
			}

			this.setState({ width: li_width + '%' });

			var step_width = move_distance;
			move_distance = move_distance * index_temp;

			var current = index + 1;

			if (current === 1 || mobile_device === true && index % 2 === 0) {
				move_distance -= 8;
			} else if (current === total_steps || mobile_device === true && index % 2 === 1) {
				move_distance += 8;
			}

			if (mobile_device) {
				vertical_level = parseInt(index / 2, 10);
				vertical_level = vertical_level * 38;
			}
			var movingTabStyle = {
				width: step_width,
				transform: 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
				transition: 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
			};
			this.setState({ movingTabStyle: movingTabStyle });
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    classes = _props.classes,
			    title = _props.title,
			    subtitle = _props.subtitle,
			    color = _props.color,
			    steps = _props.steps;

			return _react2.default.createElement(
				'div',
				{ className: classes.wizardContainer, ref: 'wizard' },
				_react2.default.createElement(
					_Card2.default,
					{ className: classes.card },
					_react2.default.createElement(
						'div',
						{ className: classes.wizardHeader },
						_react2.default.createElement(
							'h3',
							{ className: classes.title },
							title
						),
						_react2.default.createElement(
							'h5',
							{ className: classes.subtitle },
							subtitle
						)
					),
					_react2.default.createElement(
						'div',
						{ className: classes.wizardNavigation },
						_react2.default.createElement(
							'ul',
							{ className: classes.nav },
							steps.map(function (prop, key) {
								return _react2.default.createElement(
									'li',
									{
										className: classes.steps,
										key: key,
										style: { width: _this2.state.width }
									},
									_react2.default.createElement(
										'a',
										{
											className: classes.stepsAnchor,
											onClick: function onClick() {
												return _this2.navigationStepChange(key);
											}
										},
										prop.stepName
									)
								);
							})
						),
						_react2.default.createElement(
							'div',
							{
								className: classes.movingTab + ' ' + classes[color],
								style: this.state.movingTabStyle
							},
							steps[this.state.currentStep].stepName
						)
					),
					_react2.default.createElement(
						'div',
						{ className: classes.content },
						steps.map(function (prop, key) {
							var _cx;

							var stepContentClasses = (0, _classnames2.default)((_cx = {}, _defineProperty(_cx, classes.stepContentActive, _this2.state.currentStep === key), _defineProperty(_cx, classes.stepContent, _this2.state.currentStep !== key), _cx));
							return _react2.default.createElement(
								'div',
								{ className: stepContentClasses, key: key },
								_react2.default.createElement(prop.stepComponent, {
									innerRef: function innerRef(node) {
										return _this2[prop.stepId] = node;
									},
									allStates: _this2.state.allStates
								})
							);
						})
					),
					_react2.default.createElement(
						'div',
						{ className: classes.footer },
						_react2.default.createElement(
							'div',
							{ className: classes.left },
							this.state.previousButton ? _react2.default.createElement(
								_CustomButtons2.default,
								{
									className: this.props.previousButtonClasses,
									onClick: function onClick() {
										return _this2.previousButtonClick();
									}
								},
								this.props.previousButtonText
							) : null
						),
						_react2.default.createElement(
							'div',
							{ className: classes.right },
							this.state.nextButton ? _react2.default.createElement(
								_CustomButtons2.default,
								{
									color: 'rose',
									className: this.props.nextButtonClasses,
									onClick: function onClick() {
										return _this2.nextButtonClick();
									}
								},
								this.props.nextButtonText
							) : null,
							this.state.finishButton ? _react2.default.createElement(
								_CustomButtons2.default,
								{
									color: 'rose',
									className: this.finishButtonClasses,
									onClick: function onClick() {
										return _this2.finishButtonClick();
									}
								},
								this.props.finishButtonText
							) : null
						),
						_react2.default.createElement('div', { className: classes.clearfix })
					)
				)
			);
		}
	}]);

	return WizardForm;
}(_react2.default.Component);

WizardForm.defaultProps = {
	color: 'rose',
	title: 'Here should go your title',
	subtitle: 'And this would be your subtitle',
	previousButtonText: 'Previous',
	previousButtonClasses: '',
	nextButtonClasses: '',
	nextButtonText: 'Next',
	finishButtonClasses: '',
	finishButtonText: 'Finish'
};

WizardForm.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	steps: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		stepName: _propTypes2.default.string.isRequired,
		stepComponent: _propTypes2.default.func.isRequired,
		stepId: _propTypes2.default.string.isRequired
	})).isRequired,
	color: _propTypes2.default.oneOf(['primary', 'warning', 'danger', 'success', 'info', 'rose']),
	title: _propTypes2.default.string,
	subtitle: _propTypes2.default.string,
	previousButtonClasses: _propTypes2.default.string,
	previousButtonText: _propTypes2.default.string,
	nextButtonClasses: _propTypes2.default.string,
	nextButtonText: _propTypes2.default.string,
	finishButtonClasses: _propTypes2.default.string,
	finishButtonText: _propTypes2.default.string,
	finishButtonClick: _propTypes2.default.func,
	validate: _propTypes2.default.bool
};

var Wizard = (0, _withStyles2.default)(_styles2.default)(WizardForm);
exports.default = Wizard;

//# sourceMappingURL=Wizard.js.map