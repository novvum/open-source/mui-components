'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _cardBodyStyle = require('./styles/cardBodyStyle');

var _cardBodyStyle2 = _interopRequireDefault(_cardBodyStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
// nodejs library that concatenates classes

// nodejs library to set properties for components

// @material-ui/core components

// @material-ui/icons

// core components


function CardBodyComponent(_ref) {
	var _classNames;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    className = props.className,
	    children = props.children,
	    background = props.background,
	    plain = props.plain,
	    formHorizontal = props.formHorizontal,
	    pricing = props.pricing,
	    signup = props.signup,
	    color = props.color,
	    profile = props.profile,
	    calendar = props.calendar,
	    rest = _objectWithoutProperties(props, ['classes', 'className', 'children', 'background', 'plain', 'formHorizontal', 'pricing', 'signup', 'color', 'profile', 'calendar']);

	var cardBodyClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, classes.cardBody, true), _defineProperty(_classNames, classes.cardBodyBackground, background), _defineProperty(_classNames, classes.cardBodyPlain, plain), _defineProperty(_classNames, classes.cardBodyFormHorizontal, formHorizontal), _defineProperty(_classNames, classes.cardPricing, pricing), _defineProperty(_classNames, classes.cardSignup, signup), _defineProperty(_classNames, classes.cardBodyColor, color), _defineProperty(_classNames, classes.cardBodyProfile, profile), _defineProperty(_classNames, classes.cardBodyCalendar, calendar), _defineProperty(_classNames, className, className !== undefined), _classNames));
	return _react2.default.createElement(
		'div',
		Object.assign({ className: cardBodyClasses }, rest),
		children
	);
}

CardBodyComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	className: _propTypes2.default.string,
	background: _propTypes2.default.bool,
	plain: _propTypes2.default.bool,
	formHorizontal: _propTypes2.default.bool,
	pricing: _propTypes2.default.bool,
	signup: _propTypes2.default.bool,
	color: _propTypes2.default.bool,
	profile: _propTypes2.default.bool,
	calendar: _propTypes2.default.bool
};

var CardBody = (0, _withStyles2.default)(_cardBodyStyle2.default)(CardBodyComponent);
exports.default = CardBody;

//# sourceMappingURL=CardBody.js.map