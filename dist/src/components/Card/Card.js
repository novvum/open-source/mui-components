'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _cardStyle = require('./styles/cardStyle');

var _cardStyle2 = _interopRequireDefault(_cardStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
// nodejs library that concatenates classes

// nodejs library to set properties for components

// @material-ui/core components

// @material-ui/icons

// core components


function CardComponent(_ref) {
	var _classNames;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    className = props.className,
	    children = props.children,
	    plain = props.plain,
	    profile = props.profile,
	    blog = props.blog,
	    raised = props.raised,
	    background = props.background,
	    pricing = props.pricing,
	    color = props.color,
	    product = props.product,
	    testimonial = props.testimonial,
	    chart = props.chart,
	    login = props.login,
	    rest = _objectWithoutProperties(props, ['classes', 'className', 'children', 'plain', 'profile', 'blog', 'raised', 'background', 'pricing', 'color', 'product', 'testimonial', 'chart', 'login']);

	var cardClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, classes.card, true), _defineProperty(_classNames, classes.cardPlain, plain), _defineProperty(_classNames, classes.cardProfile, profile || testimonial), _defineProperty(_classNames, classes.cardBlog, blog), _defineProperty(_classNames, classes.cardRaised, raised), _defineProperty(_classNames, classes.cardBackground, background), _defineProperty(_classNames, classes.cardPricingColor, pricing && color !== undefined || pricing && background !== undefined), _defineProperty(_classNames, classes[color], color), _defineProperty(_classNames, classes.cardPricing, pricing), _defineProperty(_classNames, classes.cardProduct, product), _defineProperty(_classNames, classes.cardChart, chart), _defineProperty(_classNames, classes.cardLogin, login), _defineProperty(_classNames, className, className !== undefined), _classNames));
	return _react2.default.createElement(
		'div',
		Object.assign({ className: cardClasses }, rest),
		children
	);
}

CardComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	className: _propTypes2.default.string,
	plain: _propTypes2.default.bool,
	profile: _propTypes2.default.bool,
	blog: _propTypes2.default.bool,
	raised: _propTypes2.default.bool,
	background: _propTypes2.default.bool,
	pricing: _propTypes2.default.bool,
	testimonial: _propTypes2.default.bool,
	color: _propTypes2.default.oneOf(['primary', 'info', 'success', 'warning', 'danger', 'rose']),
	product: _propTypes2.default.bool,
	chart: _propTypes2.default.bool,
	login: _propTypes2.default.bool
};

var Card = (0, _withStyles2.default)(_cardStyle2.default)(CardComponent);
exports.default = Card;

//# sourceMappingURL=Card.js.map