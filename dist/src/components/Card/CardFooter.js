'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _cardFooterStyle = require('./styles/cardFooterStyle');

var _cardFooterStyle2 = _interopRequireDefault(_cardFooterStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
// nodejs library that concatenates classes

// nodejs library to set properties for components

// @material-ui/core components

// @material-ui/icons

// core components


function CardFooterComponent(_ref) {
	var _classNames;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    className = props.className,
	    children = props.children,
	    plain = props.plain,
	    profile = props.profile,
	    pricing = props.pricing,
	    testimonial = props.testimonial,
	    stats = props.stats,
	    chart = props.chart,
	    product = props.product,
	    rest = _objectWithoutProperties(props, ['classes', 'className', 'children', 'plain', 'profile', 'pricing', 'testimonial', 'stats', 'chart', 'product']);

	var cardFooterClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, classes.cardFooter, true), _defineProperty(_classNames, classes.cardFooterPlain, plain), _defineProperty(_classNames, classes.cardFooterProfile, profile || testimonial), _defineProperty(_classNames, classes.cardFooterPricing, pricing), _defineProperty(_classNames, classes.cardFooterTestimonial, testimonial), _defineProperty(_classNames, classes.cardFooterStats, stats), _defineProperty(_classNames, classes.cardFooterChart, chart || product), _defineProperty(_classNames, className, className !== undefined), _classNames));
	return _react2.default.createElement(
		'div',
		Object.assign({ className: cardFooterClasses }, rest),
		children
	);
}

CardFooterComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	className: _propTypes2.default.string,
	plain: _propTypes2.default.bool,
	profile: _propTypes2.default.bool,
	pricing: _propTypes2.default.bool,
	testimonial: _propTypes2.default.bool,
	stats: _propTypes2.default.bool,
	chart: _propTypes2.default.bool,
	product: _propTypes2.default.bool
};

var CardFooter = (0, _withStyles2.default)(_cardFooterStyle2.default)(CardFooterComponent);
exports.default = CardFooter;

//# sourceMappingURL=CardFooter.js.map