'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _cardIconStyle = require('./styles/cardIconStyle');

var _cardIconStyle2 = _interopRequireDefault(_cardIconStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
// nodejs library that concatenates classes

// nodejs library to set properties for components

// @material-ui/core components

// @material-ui/icons

// core components


function CardIconComponent(_ref) {
	var _classNames;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    className = props.className,
	    children = props.children,
	    color = props.color,
	    rest = _objectWithoutProperties(props, ['classes', 'className', 'children', 'color']);

	var cardIconClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, classes.cardIcon, true), _defineProperty(_classNames, classes[color + 'CardHeader'], color), _defineProperty(_classNames, className, className !== undefined), _classNames));
	return _react2.default.createElement(
		'div',
		Object.assign({ className: cardIconClasses }, rest),
		children
	);
}

CardIconComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	className: _propTypes2.default.string,
	color: _propTypes2.default.oneOf(['warning', 'success', 'danger', 'info', 'primary', 'rose'])
};

var CardIcon = (0, _withStyles2.default)(_cardIconStyle2.default)(CardIconComponent);
exports.default = CardIcon;

//# sourceMappingURL=CardIcon.js.map