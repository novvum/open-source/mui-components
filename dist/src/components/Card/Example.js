'use strict';

var _DateRange = require('@material-ui/icons/DateRange');

var _DateRange2 = _interopRequireDefault(_DateRange);

var _Store = require('@material-ui/icons/Store');

var _Store2 = _interopRequireDefault(_Store);

var _ = require('./');

var _styles = require('../styles');

var _hoverCardStyle = require('../styles/hoverCardStyle');

var _hoverCardStyle2 = _interopRequireDefault(_hoverCardStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = Object.assign({}, _hoverCardStyle2.default, {
	tooltip: _styles.tooltip,
	cardTitle: Object.assign({}, _styles.cardTitle, {
		marginTop: '0px',
		marginBottom: '3px'
	}),
	cardIconTitle: Object.assign({}, _styles.cardTitle, {
		marginTop: '15px',
		marginBottom: '0px'
	}),
	cardProductTitle: Object.assign({}, _styles.cardTitle, {
		marginTop: '0px',
		marginBottom: '3px',
		textAlign: 'center'
	}),
	cardCategory: {
		color: '#999999',
		fontSize: '14px',
		paddingTop: '10px',
		marginBottom: '0',
		marginTop: '0',
		margin: '0'
	},
	cardProductDescription: {
		textAlign: 'center',
		color: '#999999'
	},
	stats: {
		color: '#999999',
		fontSize: '12px',
		lineHeight: '22px',
		display: 'inline-flex',
		'& svg': {
			position: 'relative',
			top: '4px',
			width: '16px',
			height: '16px',
			marginRight: '3px'
		},
		'& i': {
			position: 'relative',
			top: '4px',
			fontSize: '16px',
			marginRight: '3px'
		}
	},
	productStats: {
		paddingTop: '7px',
		paddingBottom: '7px',
		margin: '0'
	},
	successText: {
		color: _styles.successColor
	},
	upArrowCardCategory: {
		width: 14,
		height: 14
	},
	underChartIcons: {
		width: '17px',
		height: '17px'
	},
	price: {
		color: 'inherit',
		'& h4': {
			marginBottom: '0px',
			marginTop: '0px'
		}
	}
});

/**
 * @render react
 * @name CardExample
 * @example
 * <center>
 * <Card>
 * 	<CardHeader color="success" stats icon>
 * 		<CardIcon color="success">
 * 			<Store />
 * 		</CardIcon>
 * 		<p className={styles.cardCategory}>Revenue</p>
 * 		<h3 className={styles.cardTitle}>$34,245</h3>
 * 	</CardHeader>
 * 	<CardFooter stats>
 * 		<div className={styles.stats}>
 * 			<DateRange />
 * 			Last 24 Hours
 * 		</div>
 * 	</CardFooter>
 * </Card>
 * </center>
 */

//# sourceMappingURL=Example.js.map