'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../../styles');

var cardTextStyle = {
	cardText: {
		float: 'none',
		display: 'inline-block',
		marginRight: '0',
		borderRadius: '3px',
		backgroundColor: '#999999',
		padding: '15px',
		marginTop: '-20px'
	},
	warningCardHeader: _styles.warningCardHeader,
	successCardHeader: _styles.successCardHeader,
	dangerCardHeader: _styles.dangerCardHeader,
	infoCardHeader: _styles.infoCardHeader,
	primaryCardHeader: _styles.primaryCardHeader,
	roseCardHeader: _styles.roseCardHeader
};

exports.default = cardTextStyle;

//# sourceMappingURL=cardTextStyle.js.map