'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../../styles');

var cardIconStyle = {
	cardIcon: {
		'&$warningCardHeader,&$successCardHeader,&$dangerCardHeader,&$infoCardHeader,&$primaryCardHeader,&$roseCardHeader': {
			borderRadius: '3px',
			backgroundColor: '#999',
			padding: '15px',
			marginTop: '-20px',
			marginRight: '15px',
			float: 'left'
		}
	},
	warningCardHeader: _styles.warningCardHeader,
	successCardHeader: _styles.successCardHeader,
	dangerCardHeader: _styles.dangerCardHeader,
	infoCardHeader: _styles.infoCardHeader,
	primaryCardHeader: _styles.primaryCardHeader,
	roseCardHeader: _styles.roseCardHeader
};

exports.default = cardIconStyle;

//# sourceMappingURL=cardIconStyle.js.map