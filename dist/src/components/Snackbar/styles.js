'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var snackbarContentStyle = {
	root: Object.assign({}, _styles.defaultFont, {
		flexWrap: 'unset',
		position: 'relative',
		padding: '20px 15px',
		lineHeight: '20px',
		marginBottom: '20px',
		fontSize: '14px',
		backgroundColor: 'white',
		color: '#555555',
		borderRadius: '3px',
		boxShadow: '0 12px 20px -10px rgba(255, 255, 255, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(255, 255, 255, 0.2)'
	}),
	top20: {
		top: '20px'
	},
	top40: {
		top: '40px'
	},
	info: Object.assign({
		backgroundColor: '#00d3ee',
		color: '#ffffff'
	}, _styles.infoBoxShadow),
	success: Object.assign({
		backgroundColor: '#5cb860',
		color: '#ffffff'
	}, _styles.successBoxShadow),
	warning: Object.assign({
		backgroundColor: '#ffa21a',
		color: '#ffffff'
	}, _styles.warningBoxShadow),
	danger: Object.assign({
		backgroundColor: '#f55a4e',
		color: '#ffffff'
	}, _styles.dangerBoxShadow),
	primary: Object.assign({
		backgroundColor: '#af2cc5',
		color: '#ffffff'
	}, _styles.primaryBoxShadow),
	rose: Object.assign({
		backgroundColor: '#eb3573',
		color: '#ffffff'
	}, _styles.roseBoxShadow),
	message: {
		padding: '0',
		display: 'block',
		maxWidth: '89%'
	},
	close: {
		width: '11px',
		height: '11px'
	},
	iconButton: {
		width: '24px',
		height: '24px'
	},
	icon: {
		// display: "block",
		// left: "15px",
		// position: "absolute",
		// top: "50%",
		// marginTop: "-15px",
		width: '38px',
		height: '38px',
		display: 'block',
		left: '15px',
		position: 'absolute',
		marginTop: '-39px',
		fontSize: '20px',
		backgroundColor: '#FFFFFF',
		padding: '9px',
		borderRadius: '50%',
		maxWidth: '38px',
		boxShadow: '0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)'
	},
	infoIcon: {
		color: '#00d3ee'
	},
	successIcon: {
		color: '#5cb860'
	},
	warningIcon: {
		color: '#ffa21a'
	},
	dangerIcon: {
		color: '#f55a4e'
	},
	primaryIcon: {
		color: '#af2cc5'
	},
	roseIcon: {
		color: '#eb3573'
	},
	iconMessage: {
		paddingLeft: '50px',
		display: 'block'
	}
}; // ##############################
// // // SnackbarContent styles
// #############################

exports.default = snackbarContentStyle;

//# sourceMappingURL=styles.js.map