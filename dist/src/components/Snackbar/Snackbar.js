'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Snackbar = require('@material-ui/core/Snackbar');

var _Snackbar2 = _interopRequireDefault(_Snackbar);

var _IconButton = require('@material-ui/core/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Close = require('@material-ui/icons/Close');

var _Close2 = _interopRequireDefault(_Close);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


// @material-ui/icons


function SnackbarComponent(_ref) {
	var _cx2;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    message = props.message,
	    color = props.color,
	    close = props.close,
	    icon = props.icon,
	    place = props.place,
	    open = props.open;

	var action = [];
	var messageClasses = (0, _classnames2.default)(_defineProperty({}, classes.iconMessage, icon !== undefined));
	if (close !== undefined) {
		action = [_react2.default.createElement(
			_IconButton2.default,
			{
				className: classes.iconButton,
				key: 'close',
				'aria-label': 'Close',
				color: 'inherit',
				onClick: function onClick() {
					return props.closeNotification();
				}
			},
			_react2.default.createElement(_Close2.default, { className: classes.close })
		)];
	}
	var iconClasses = (0, _classnames2.default)((_cx2 = {}, _defineProperty(_cx2, classes.icon, classes.icon), _defineProperty(_cx2, classes.infoIcon, color === 'info'), _defineProperty(_cx2, classes.successIcon, color === 'success'), _defineProperty(_cx2, classes.warningIcon, color === 'warning'), _defineProperty(_cx2, classes.dangerIcon, color === 'danger'), _defineProperty(_cx2, classes.primaryIcon, color === 'primary'), _defineProperty(_cx2, classes.roseIcon, color === 'rose'), _cx2));
	return _react2.default.createElement(_Snackbar2.default, {
		classes: {
			anchorOriginTopCenter: classes.top20,
			anchorOriginTopRight: classes.top40,
			anchorOriginTopLeft: classes.top40
		},
		anchorOrigin: {
			vertical: place.indexOf('t') === -1 ? 'bottom' : 'top',
			horizontal: place.indexOf('l') !== -1 ? 'left' : place.indexOf('c') !== -1 ? 'center' : 'right'
		},
		open: open,
		message: _react2.default.createElement(
			'div',
			null,
			icon !== undefined ? _react2.default.createElement(props.icon, { className: iconClasses }) : null,
			_react2.default.createElement(
				'span',
				{ className: messageClasses },
				message
			)
		),
		action: action,
		ContentProps: {
			classes: {
				root: classes.root + ' ' + classes[color],
				message: classes.message
			}
		}
	});
}

SnackbarComponent.defaultProps = {
	color: 'info'
};

SnackbarComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	message: _propTypes2.default.node.isRequired,
	color: _propTypes2.default.oneOf(['info', 'success', 'warning', 'danger', 'primary', 'rose']),
	close: _propTypes2.default.bool,
	icon: _propTypes2.default.func,
	place: _propTypes2.default.oneOf(['tl', 'tr', 'tc', 'br', 'bl', 'bc']),
	open: _propTypes2.default.bool
};

var Snackbar = (0, _withStyles2.default)(_styles2.default)(SnackbarComponent);
exports.default = Snackbar;

//# sourceMappingURL=Snackbar.js.map