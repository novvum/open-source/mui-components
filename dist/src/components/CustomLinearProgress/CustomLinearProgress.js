'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _LinearProgress = require('@material-ui/core/LinearProgress');

var _LinearProgress2 = _interopRequireDefault(_LinearProgress);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function CustomLinearProgressComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    color = props.color,
	    rest = _objectWithoutProperties(props, ['classes', 'color']);

	return _react2.default.createElement(_LinearProgress2.default, Object.assign({}, rest, {
		classes: {
			root: classes.root + ' ' + classes[color + 'Background'],
			bar: classes.bar + ' ' + classes[color]
		}
	}));
}

CustomLinearProgressComponent.defaultProps = {
	color: 'gray'
};

CustomLinearProgressComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	color: _propTypes2.default.oneOf(['primary', 'warning', 'danger', 'success', 'info', 'rose', 'gray'])
};

var CustomLinearProgress = (0, _withStyles2.default)(_styles2.default)(CustomLinearProgressComponent);
exports.default = CustomLinearProgress;

//# sourceMappingURL=CustomLinearProgress.js.map