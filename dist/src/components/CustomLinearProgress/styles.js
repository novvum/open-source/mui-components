'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var customLinearProgressStyle = {
	root: {
		height: '4px',
		marginBottom: '20px',
		overflow: 'hidden'
	},
	bar: {
		height: '4px'
	},
	primary: {
		backgroundColor: _styles.primaryColor
	},
	warning: {
		backgroundColor: _styles.warningColor
	},
	danger: {
		backgroundColor: _styles.dangerColor
	},
	success: {
		backgroundColor: _styles.successColor
	},
	info: {
		backgroundColor: _styles.infoColor
	},
	rose: {
		backgroundColor: _styles.roseColor
	},
	gray: {
		backgroundColor: _styles.grayColor
	},
	primaryBackground: {
		background: 'rgba(156, 39, 176, 0.2)'
	},
	warningBackground: {
		background: 'rgba(255, 152, 0, 0.2)'
	},
	dangerBackground: {
		background: 'rgba(244, 67, 54, 0.2)'
	},
	successBackground: {
		background: 'rgba(76, 175, 80, 0.2)'
	},
	infoBackground: {
		background: 'rgba(0, 188, 212, 0.2)'
	},
	roseBackground: {
		background: 'rgba(233, 30, 99, 0.2)'
	},
	grayBackground: {
		background: 'rgba(221, 221, 221, 0.2)'
	}
}; // ##############################
// // // CustomLinearProgress component styles
// #############################

exports.default = customLinearProgressStyle;

//# sourceMappingURL=styles.js.map