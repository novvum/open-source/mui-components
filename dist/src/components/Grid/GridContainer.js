'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = require('@material-ui/core/Grid');

var _Grid2 = _interopRequireDefault(_Grid);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


var style = {
	grid: {
		margin: '0 -15px',
		width: 'calc(100% + 30px)'
		// '&:before,&:after':{
		//   display: 'table',
		//   content: '" "',
		// },
		// '&:after':{
		//   clear: 'both',
		// }
	}
};

function GridContainerComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    children = props.children,
	    className = props.className,
	    rest = _objectWithoutProperties(props, ['classes', 'children', 'className']);

	return _react2.default.createElement(
		_Grid2.default,
		Object.assign({ container: true }, rest, { className: classes.grid + ' ' + className }),
		children
	);
}

var GridContainer = (0, _withStyles2.default)(style)(GridContainerComponent);
exports.default = GridContainer;

//# sourceMappingURL=GridContainer.js.map