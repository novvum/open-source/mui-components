'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ExpansionPanel = require('@material-ui/core/ExpansionPanel');

var _ExpansionPanel2 = _interopRequireDefault(_ExpansionPanel);

var _ExpansionPanelSummary = require('@material-ui/core/ExpansionPanelSummary');

var _ExpansionPanelSummary2 = _interopRequireDefault(_ExpansionPanelSummary);

var _ExpansionPanelDetails = require('@material-ui/core/ExpansionPanelDetails');

var _ExpansionPanelDetails2 = _interopRequireDefault(_ExpansionPanelDetails);

var _ExpandMore = require('@material-ui/icons/ExpandMore');

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // @material-ui/icons


// @material-ui/core components


var AccordionComponent = function (_React$Component) {
	_inherits(AccordionComponent, _React$Component);

	function AccordionComponent(props) {
		_classCallCheck(this, AccordionComponent);

		var _this = _possibleConstructorReturn(this, (AccordionComponent.__proto__ || Object.getPrototypeOf(AccordionComponent)).call(this, props));

		_this.handleChange = function (panel) {
			return function (event, expanded) {
				_this.setState({
					active: expanded ? panel : -1
				});
			};
		};

		_this.state = {
			active: props.active
		};
		return _this;
	}

	_createClass(AccordionComponent, [{
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    classes = _props.classes,
			    collapses = _props.collapses;

			return _react2.default.createElement(
				'div',
				{ className: classes.root },
				collapses.map(function (prop, key) {
					return _react2.default.createElement(
						_ExpansionPanel2.default,
						{
							expanded: _this2.state.active === key,
							onChange: _this2.handleChange(key),
							key: key,
							classes: {
								root: classes.expansionPanel,
								expanded: classes.expansionPanelExpanded
							}
						},
						_react2.default.createElement(
							_ExpansionPanelSummary2.default,
							{
								expandIcon: _react2.default.createElement(_ExpandMore2.default, null),
								classes: {
									root: classes.expansionPanelSummary,
									expanded: classes.expansionPanelSummaryExpaned,
									content: classes.expansionPanelSummaryContent,
									expandIcon: classes.expansionPanelSummaryExpandIcon
								}
							},
							_react2.default.createElement(
								'h4',
								{ className: classes.title },
								prop.title
							)
						),
						_react2.default.createElement(
							_ExpansionPanelDetails2.default,
							{ className: classes.expansionPanelDetails },
							prop.content
						)
					);
				})
			);
		}
	}]);

	return AccordionComponent;
}(_react2.default.Component);

AccordionComponent.defaultProps = {
	active: -1
};

AccordionComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	// index of the default active collapse
	active: _propTypes2.default.number,
	collapses: _propTypes2.default.arrayOf(_propTypes2.default.shape({
		title: _propTypes2.default.string,
		content: _propTypes2.default.node
	})).isRequired
};

var Accordion = (0, _withStyles2.default)(_styles2.default)(AccordionComponent);

exports.default = Accordion;

/**
 * @render react
 * @name Accordion
 * @example
 * <Accordion
 * 	active={0}
 * 	collapses={[
 * 		{
 * 			title: "SPIDER-MAN",
 * 			content:
 * 				"Bitten by a radioactive spider, Peter Parker’s arachnid abilities give him amazing powers he uses to help others, while his personal life continues to offer plenty of obstacles."
 * 		},
 * 		{
 * 			title: "BLACK PANTHER",
 * 			content:
 * 				"T’Challa is the king of the secretive and highly advanced African nation of Wakanda as well as the powerful warrior known as the Black Panther."
 * 		},
 * 		{
 * 			title: "IRON MAN",
 * 			content:
 * 				"Genius. Billionaire. Playboy. Philanthropist. Tony Stark's confidence is only matched by his high-flying abilities as the hero called Iron Man."
 * 		}
 * 	]}
 * />
 */

//# sourceMappingURL=Accordion.js.map