'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var accordionStyle = function accordionStyle(theme) {
	var _$expansionPanelSum, _expansionPanelSummar;

	return {
		root: {
			flexGrow: 1,
			marginBottom: '20px'
		},
		expansionPanel: {
			boxShadow: 'none',
			'&:before': {
				display: 'none !important'
			}
		},
		expansionPanelExpanded: {
			margin: '0'
		},
		expansionPanelSummary: {
			minHeight: 'auto !important',
			backgroundColor: 'transparent',
			borderBottom: '1px solid #ddd',
			padding: '25px 10px 5px 0px',
			borderTopLeftRadius: '3px',
			borderTopRightRadius: '3px',
			color: '#3C4858',
			'&:hover': {
				color: _styles.primaryColor
			}
		},
		expansionPanelSummaryExpaned: {
			color: _styles.primaryColor,
			'& $expansionPanelSummaryExpandIcon': (_$expansionPanelSum = {}, _defineProperty(_$expansionPanelSum, theme.breakpoints.up('md'), {
				top: 'auto !important'
			}), _defineProperty(_$expansionPanelSum, 'transform', 'rotate(180deg)'), _defineProperty(_$expansionPanelSum, theme.breakpoints.down('sm'), {
				top: '10px !important'
			}), _$expansionPanelSum)
		},
		expansionPanelSummaryContent: {
			margin: '0 !important'
		},
		expansionPanelSummaryExpandIcon: (_expansionPanelSummar = {}, _defineProperty(_expansionPanelSummar, theme.breakpoints.up('md'), {
			top: 'auto !important'
		}), _defineProperty(_expansionPanelSummar, 'transform', 'rotate(0deg)'), _defineProperty(_expansionPanelSummar, 'color', 'inherit'), _defineProperty(_expansionPanelSummar, theme.breakpoints.down('sm'), {
			top: '10px !important'
		}), _expansionPanelSummar),
		expansionPanelSummaryExpandIconExpanded: {},
		title: {
			fontSize: '15px',
			fontWeight: 'bolder',
			marginTop: '0',
			marginBottom: '0',
			color: 'inherit'
		},
		expansionPanelDetails: {
			padding: '15px 0px 5px'
		}
	};
};

exports.default = accordionStyle;

//# sourceMappingURL=styles.js.map