'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function HeadingComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var textAlign = props.textAlign,
	    category = props.category,
	    title = props.title,
	    classes = props.classes;

	var heading = classes.heading + ' ' + (0, _classnames2.default)(_defineProperty({}, classes[textAlign + 'TextAlign'], textAlign !== undefined));
	if (title !== undefined || category !== undefined) {
		return _react2.default.createElement(
			'div',
			{ className: heading },
			title !== undefined ? _react2.default.createElement(
				'h3',
				{ className: classes.title },
				title
			) : null,
			category !== undefined ? _react2.default.createElement(
				'p',
				{ className: classes.category },
				category
			) : null
		);
	}
	return null;
}

HeadingComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	title: _propTypes2.default.node,
	category: _propTypes2.default.node,
	textAlign: _propTypes2.default.oneOf(['right', 'left', 'center'])
};

var Heading = (0, _withStyles2.default)(_styles2.default)(HeadingComponent);
exports.default = Heading;

//# sourceMappingURL=Heading.js.map