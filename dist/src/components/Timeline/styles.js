'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // ##############################
// // // Timeline component styles
// #############################

var timelineStyle = function timelineStyle(theme) {
	var _timeline, _timelineBadge, _timelinePanel;

	return {
		timeline: (_timeline = {}, _defineProperty(_timeline, theme.breakpoints.down('sm'), {
			'&:before': {
				left: '5% !important'
			}
		}), _defineProperty(_timeline, 'listStyle', 'none'), _defineProperty(_timeline, 'padding', '20px 0 20px'), _defineProperty(_timeline, 'position', 'relative'), _defineProperty(_timeline, 'marginTop', '30px'), _defineProperty(_timeline, '&:before', {
			top: '50px',
			bottom: '0',
			position: 'absolute',
			content: '" "',
			width: '3px',
			backgroundColor: '#E5E5E5',
			left: '50%',
			marginLeft: '-1px'
		}), _timeline),
		timelineSimple: {
			marginTop: '30px',
			padding: '0 0 20px',
			'&:before': {
				left: '5%'
			}
		},
		item: {
			marginBottom: '20px',
			position: 'relative',
			'&:before,&:after': {
				content: '" "',
				display: 'table'
			},
			'&:after': {
				clear: 'both'
			}
		},
		timelineBadge: (_timelineBadge = {}, _defineProperty(_timelineBadge, theme.breakpoints.down('sm'), {
			left: '5% !important'
		}), _defineProperty(_timelineBadge, 'color', '#FFFFFF'), _defineProperty(_timelineBadge, 'width', '50px'), _defineProperty(_timelineBadge, 'height', '50px'), _defineProperty(_timelineBadge, 'lineHeight', '51px'), _defineProperty(_timelineBadge, 'fontSize', '1.4em'), _defineProperty(_timelineBadge, 'textAlign', 'center'), _defineProperty(_timelineBadge, 'position', 'absolute'), _defineProperty(_timelineBadge, 'top', '16px'), _defineProperty(_timelineBadge, 'left', '50%'), _defineProperty(_timelineBadge, 'marginLeft', '-24px'), _defineProperty(_timelineBadge, 'zIndex', '100'), _defineProperty(_timelineBadge, 'borderTopRightRadius', '50%'), _defineProperty(_timelineBadge, 'borderTopLeftRadius', '50%'), _defineProperty(_timelineBadge, 'borderBottomRightRadius', '50%'), _defineProperty(_timelineBadge, 'borderBottomLeftRadius', '50%'), _timelineBadge),
		timelineSimpleBadge: {
			left: '5%'
		},
		info: {
			backgroundColor: _styles.infoColor,
			boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(0, 188, 212, 0.4)'
		},
		success: {
			backgroundColor: _styles.successColor,
			boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(76, 175, 80, 0.4)'
		},
		danger: {
			backgroundColor: _styles.dangerColor,
			boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(244, 67, 54, 0.4)'
		},
		warning: {
			backgroundColor: _styles.warningColor,
			boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(255, 152, 0, 0.4)'
		},
		primary: {
			backgroundColor: _styles.primaryColor,
			boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(156, 39, 176, 0.4)'
		},
		badgeIcon: {
			width: '24px',
			height: '51px'
		},
		timelinePanel: (_timelinePanel = {}, _defineProperty(_timelinePanel, theme.breakpoints.down('sm'), {
			float: 'right !important',
			width: '86% !important',
			'&:before': {
				borderLeftWidth: '0 !important',
				borderRightWidth: '15px !important',
				left: '-15px !important',
				right: 'auto !important'
			},
			'&:after': {
				borderLeftWidth: '0 !important',
				borderRightWidth: '14px !important',
				left: '-14px !important',
				right: 'auto !important'
			}
		}), _defineProperty(_timelinePanel, 'width', '45%'), _defineProperty(_timelinePanel, 'float', 'left'), _defineProperty(_timelinePanel, 'padding', '20px'), _defineProperty(_timelinePanel, 'marginBottom', '20px'), _defineProperty(_timelinePanel, 'position', 'relative'), _defineProperty(_timelinePanel, 'boxShadow', '0 1px 4px 0 rgba(0, 0, 0, 0.14)'), _defineProperty(_timelinePanel, 'borderRadius', '6px'), _defineProperty(_timelinePanel, 'color', 'rgba(0, 0, 0, 0.87)'), _defineProperty(_timelinePanel, 'background', '#fff'), _defineProperty(_timelinePanel, '&:before', {
			position: 'absolute',
			top: '26px',
			right: '-15px',
			display: 'inline-block',
			borderTop: '15px solid transparent',
			borderLeft: '15px solid #e4e4e4',
			borderRight: '0 solid #e4e4e4',
			borderBottom: '15px solid transparent',
			content: '" "'
		}), _defineProperty(_timelinePanel, '&:after', {
			position: 'absolute',
			top: '27px',
			right: '-14px',
			display: 'inline-block',
			borderTop: '14px solid transparent',
			borderLeft: '14px solid #FFFFFF',
			borderRight: '0 solid #FFFFFF',
			borderBottom: '14px solid transparent',
			content: '" "'
		}), _timelinePanel),
		timelineSimplePanel: {
			width: '86%'
		},
		timelinePanelInverted: _defineProperty({}, theme.breakpoints.up('sm'), {
			float: 'right',
			backgroundColor: '#FFFFFF',
			'&:before': {
				borderLeftWidth: '0',
				borderRightWidth: '15px',
				left: '-15px',
				right: 'auto'
			},
			'&:after': {
				borderLeftWidth: '0',
				borderRightWidth: '14px',
				left: '-14px',
				right: 'auto'
			}
		}),
		timelineHeading: {
			marginBottom: '15px'
		},
		timelineBody: {
			fontSize: '14px',
			lineHeight: '21px'
		},
		timelineFooter: {
			zIndex: '1000',
			position: 'relative',
			float: 'left'
		},
		footerTitle: {
			color: '#333333',
			fontWeight: '400',
			margin: '10px 0px 0px'
		},
		footerLine: {
			marginTop: '10px',
			marginBottom: '5px'
		}
	};
};

exports.default = timelineStyle;

//# sourceMappingURL=styles.js.map