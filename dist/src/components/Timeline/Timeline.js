'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Badge = require('../Badge');

var _Badge2 = _interopRequireDefault(_Badge);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


// core components


function TLine(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    stories = props.stories,
	    simple = props.simple;

	var timelineClass = classes.timeline + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.timelineSimple, simple));
	return _react2.default.createElement(
		'ul',
		{ className: timelineClass },
		stories.map(function (prop, key) {
			var _cx2;

			var panelClasses = classes.timelinePanel + ' ' + (0, _classnames2.default)((_cx2 = {}, _defineProperty(_cx2, classes.timelinePanelInverted, prop.inverted), _defineProperty(_cx2, classes.timelineSimplePanel, simple), _cx2));
			var timelineBadgeClasses = classes.timelineBadge + ' ' + classes[prop.badgeColor] + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.timelineSimpleBadge, simple));
			return _react2.default.createElement(
				'li',
				{ className: classes.item, key: key },
				prop.badgeIcon ? _react2.default.createElement(
					'div',
					{ className: timelineBadgeClasses },
					_react2.default.createElement(prop.badgeIcon, { className: classes.badgeIcon })
				) : null,
				_react2.default.createElement(
					'div',
					{ className: panelClasses },
					prop.title ? _react2.default.createElement(
						'div',
						{ className: classes.timelineHeading },
						_react2.default.createElement(
							_Badge2.default,
							{ color: prop.titleColor },
							prop.title
						)
					) : null,
					_react2.default.createElement(
						'div',
						{ className: classes.timelineBody },
						prop.body
					),
					prop.footerTitle ? _react2.default.createElement(
						'h6',
						{ className: classes.footerTitle },
						prop.footerTitle
					) : null,
					prop.footer ? _react2.default.createElement('hr', { className: classes.footerLine }) : null,
					prop.footer ? _react2.default.createElement(
						'div',
						{ className: classes.timelineFooter },
						prop.footer
					) : null
				)
			);
		})
	);
}

TLine.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	stories: _propTypes2.default.arrayOf(_propTypes2.default.object).isRequired,
	simple: _propTypes2.default.bool
};

var Timeline = (0, _withStyles2.default)(_styles2.default)(TLine);
exports.default = Timeline;

//# sourceMappingURL=Timeline.js.map