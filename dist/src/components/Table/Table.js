'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Table = require('@material-ui/core/Table');

var _Table2 = _interopRequireDefault(_Table);

var _TableBody = require('@material-ui/core/TableBody');

var _TableBody2 = _interopRequireDefault(_TableBody);

var _TableCell = require('@material-ui/core/TableCell');

var _TableCell2 = _interopRequireDefault(_TableCell);

var _TableHead = require('@material-ui/core/TableHead');

var _TableHead2 = _interopRequireDefault(_TableHead);

var _TableRow = require('@material-ui/core/TableRow');

var _TableRow2 = _interopRequireDefault(_TableRow);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function CustomTable(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    tableHead = props.tableHead,
	    tableData = props.tableData,
	    tableHeaderColor = props.tableHeaderColor,
	    hover = props.hover,
	    colorsColls = props.colorsColls,
	    coloredColls = props.coloredColls,
	    customCellClasses = props.customCellClasses,
	    customClassesForCells = props.customClassesForCells,
	    striped = props.striped,
	    tableShopping = props.tableShopping,
	    customHeadCellClasses = props.customHeadCellClasses,
	    customHeadClassesForCells = props.customHeadClassesForCells;

	return _react2.default.createElement(
		'div',
		{ className: classes.tableResponsive },
		_react2.default.createElement(
			_Table2.default,
			{ className: classes.table },
			tableHead !== undefined ? _react2.default.createElement(
				_TableHead2.default,
				{ className: classes[tableHeaderColor] },
				_react2.default.createElement(
					_TableRow2.default,
					{ className: classes.tableRow },
					tableHead.map(function (prop, key) {
						var _cx;

						var tableCellClasses = classes.tableHeadCell + ' ' + classes.tableCell + ' ' + (0, _classnames2.default)((_cx = {}, _defineProperty(_cx, customHeadCellClasses[customHeadClassesForCells.indexOf(key)], customHeadClassesForCells.indexOf(key) !== -1), _defineProperty(_cx, classes.tableShoppingHead, tableShopping), _defineProperty(_cx, classes.tableHeadFontSize, !tableShopping), _cx));
						return _react2.default.createElement(
							_TableCell2.default,
							{ className: tableCellClasses, key: key },
							prop
						);
					})
				)
			) : null,
			_react2.default.createElement(
				_TableBody2.default,
				null,
				tableData.map(function (prop, key) {
					var _cx2;

					var rowColor = '';
					var rowColored = false;
					if (prop.color !== undefined) {
						rowColor = prop.color;
						rowColored = true;
						prop = prop.data;
					}
					var tableRowClasses = (0, _classnames2.default)((_cx2 = {}, _defineProperty(_cx2, classes.tableRowHover, hover), _defineProperty(_cx2, classes[rowColor + 'Row'], rowColored), _defineProperty(_cx2, classes.tableStripedRow, striped && key % 2 === 0), _cx2));
					if (prop.total) {
						return _react2.default.createElement(
							_TableRow2.default,
							{ key: key, hover: hover, className: tableRowClasses },
							_react2.default.createElement(_TableCell2.default, {
								className: classes.tableCell,
								colSpan: prop.colspan
							}),
							_react2.default.createElement(
								_TableCell2.default,
								{
									className: classes.tableCell + ' ' + classes.tableCellTotal
								},
								'Total'
							),
							_react2.default.createElement(
								_TableCell2.default,
								{
									className: classes.tableCell + ' ' + classes.tableCellAmount
								},
								prop.amount
							),
							tableHead.length - (prop.colspan - 0 + 2) > 0 ? _react2.default.createElement(_TableCell2.default, {
								className: classes.tableCell,
								colSpan: tableHead.length - (prop.colspan - 0 + 2)
							}) : null
						);
					}
					if (prop.purchase) {
						return _react2.default.createElement(
							_TableRow2.default,
							{ key: key, hover: hover, className: tableRowClasses },
							_react2.default.createElement(_TableCell2.default, {
								className: classes.tableCell,
								colSpan: prop.colspan
							}),
							_react2.default.createElement(
								_TableCell2.default,
								{
									className: classes.tableCell + ' ' + classes.right,
									colSpan: prop.col.colspan
								},
								prop.col.text
							)
						);
					}
					return _react2.default.createElement(
						_TableRow2.default,
						{
							key: key,
							hover: hover,
							className: classes.tableRow + ' ' + tableRowClasses
						},
						prop.map(function (prop, key) {
							var _cx3;

							var tableCellClasses = classes.tableCell + ' ' + (0, _classnames2.default)((_cx3 = {}, _defineProperty(_cx3, classes[colorsColls[coloredColls.indexOf(key)]], coloredColls.indexOf(key) !== -1), _defineProperty(_cx3, customCellClasses[customClassesForCells.indexOf(key)], customClassesForCells.indexOf(key) !== -1), _cx3));
							return _react2.default.createElement(
								_TableCell2.default,
								{ className: tableCellClasses, key: key },
								prop
							);
						})
					);
				})
			)
		)
	);
}

CustomTable.defaultProps = {
	tableHeaderColor: 'gray',
	hover: false,
	colorsColls: [],
	coloredColls: [],
	striped: false,
	customCellClasses: [],
	customClassesForCells: [],
	customHeadCellClasses: [],
	customHeadClassesForCells: []
};

CustomTable.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	tableHeaderColor: _propTypes2.default.oneOf(['warning', 'primary', 'danger', 'success', 'info', 'rose', 'gray']),
	tableHead: _propTypes2.default.arrayOf(_propTypes2.default.string),
	// Of(PropTypes.arrayOf(PropTypes.node)) || Of(PropTypes.object),
	tableData: _propTypes2.default.array,
	hover: _propTypes2.default.bool,
	coloredColls: _propTypes2.default.arrayOf(_propTypes2.default.number),
	// Of(["warning","primary","danger","success","info","rose","gray"]) - colorsColls
	colorsColls: _propTypes2.default.array,
	customCellClasses: _propTypes2.default.arrayOf(_propTypes2.default.string),
	customClassesForCells: _propTypes2.default.arrayOf(_propTypes2.default.number),
	customHeadCellClasses: _propTypes2.default.arrayOf(_propTypes2.default.string),
	customHeadClassesForCells: _propTypes2.default.arrayOf(_propTypes2.default.number),
	striped: _propTypes2.default.bool,
	// this will cause some changes in font
	tableShopping: _propTypes2.default.bool
};

var CTable = (0, _withStyles2.default)(_styles2.default)(CustomTable);
exports.default = CTable;

//# sourceMappingURL=Table.js.map