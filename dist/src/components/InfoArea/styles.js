'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var infoStyle = {
	infoArea: {
		maxWidth: '360px',
		margin: '0 auto',
		padding: '0px'
	},
	iconWrapper: {
		float: 'left',
		marginTop: '24px',
		marginRight: '10px'
	},
	primary: {
		color: _styles.primaryColor
	},
	warning: {
		color: _styles.warningColor
	},
	danger: {
		color: _styles.dangerColor
	},
	success: {
		color: _styles.successColor
	},
	info: {
		color: _styles.infoColor
	},
	rose: {
		color: _styles.roseColor
	},
	gray: {
		color: _styles.grayColor
	},
	icon: {
		width: '36px',
		height: '36px'
	},
	descriptionWrapper: {
		color: _styles.grayColor,
		overflow: 'hidden'
	},
	title: {
		color: '#3C4858',
		margin: '30px 0 15px',
		textDecoration: 'none',
		fontSize: '18px'
	},
	description: {
		color: _styles.grayColor,
		overflow: 'hidden',
		marginTop: '0px',
		fontSize: '14px'
	}
}; // ##############################
// // // Info component styles
// #############################

exports.default = infoStyle;

//# sourceMappingURL=styles.js.map