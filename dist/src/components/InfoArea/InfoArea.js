'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function InfoAreaComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    title = props.title,
	    description = props.description,
	    iconColor = props.iconColor;

	return _react2.default.createElement(
		'div',
		{ className: classes.infoArea },
		_react2.default.createElement(
			'div',
			{ className: classes.iconWrapper + ' ' + classes[iconColor] },
			_react2.default.createElement(props.icon, { className: classes.icon })
		),
		_react2.default.createElement(
			'div',
			{ className: classes.descriptionWrapper },
			_react2.default.createElement(
				'h4',
				{ className: classes.title },
				title
			),
			_react2.default.createElement(
				'p',
				{ className: classes.description },
				description
			)
		)
	);
}

InfoAreaComponent.defaultProps = {
	iconColor: 'gray'
};

InfoAreaComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	icon: _propTypes2.default.func.isRequired,
	title: _propTypes2.default.string.isRequired,
	description: _propTypes2.default.string.isRequired,
	iconColor: _propTypes2.default.oneOf(['primary', 'warning', 'danger', 'success', 'info', 'rose', 'gray'])
};

var InfoArea = (0, _withStyles2.default)(_styles2.default)(InfoAreaComponent);
exports.default = InfoArea;

//# sourceMappingURL=InfoArea.js.map