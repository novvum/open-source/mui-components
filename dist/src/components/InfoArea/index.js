'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _InfoArea = require('./InfoArea');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_InfoArea).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//# sourceMappingURL=index.js.map