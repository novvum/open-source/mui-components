'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _FormControl = require('@material-ui/core/FormControl');

var _FormControl2 = _interopRequireDefault(_FormControl);

var _InputLabel = require('@material-ui/core/InputLabel');

var _InputLabel2 = _interopRequireDefault(_InputLabel);

var _Input = require('@material-ui/core/Input');

var _Input2 = _interopRequireDefault(_Input);

var _Check = require('@material-ui/icons/Check');

var _Check2 = _interopRequireDefault(_Check);

var _Clear = require('@material-ui/icons/Clear');

var _Clear2 = _interopRequireDefault(_Clear);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
// nodejs library to set properties for components

// nodejs library that concatenates classes


function CustomInputComponent(_ref) {
	var _classNames, _classNames2, _classNames4;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    formControlProps = props.formControlProps,
	    labelText = props.labelText,
	    id = props.id,
	    labelProps = props.labelProps,
	    inputProps = props.inputProps,
	    error = props.error,
	    white = props.white,
	    inputRootCustomClasses = props.inputRootCustomClasses,
	    success = props.success;


	var labelClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, ' ' + classes.labelRootError, error), _defineProperty(_classNames, ' ' + classes.labelRootSuccess, success && !error), _classNames));
	var underlineClasses = (0, _classnames2.default)((_classNames2 = {}, _defineProperty(_classNames2, classes.underlineError, error), _defineProperty(_classNames2, classes.underlineSuccess, success && !error), _defineProperty(_classNames2, classes.underline, true), _defineProperty(_classNames2, classes.whiteUnderline, white), _classNames2));
	var marginTop = (0, _classnames2.default)(_defineProperty({}, inputRootCustomClasses, inputRootCustomClasses !== undefined));
	var inputClasses = (0, _classnames2.default)((_classNames4 = {}, _defineProperty(_classNames4, classes.input, true), _defineProperty(_classNames4, classes.whiteInput, white), _classNames4));
	var formControlClasses;
	if (formControlProps !== undefined) {
		formControlClasses = (0, _classnames2.default)(formControlProps.className, classes.formControl);
	} else {
		formControlClasses = classes.formControl;
	}
	var feedbackClasses = classes.feedback;
	if (inputProps !== undefined) {
		if (inputProps.endAdornment !== undefined) {
			feedbackClasses = feedbackClasses + ' ' + classes.feedbackRight;
		}
	}
	return _react2.default.createElement(
		_FormControl2.default,
		Object.assign({}, formControlProps, { className: formControlClasses }),
		labelText !== undefined ? _react2.default.createElement(
			_InputLabel2.default,
			Object.assign({
				className: classes.labelRoot + ' ' + labelClasses,
				htmlFor: id
			}, labelProps),
			labelText
		) : null,
		_react2.default.createElement(_Input2.default, Object.assign({
			classes: {
				input: inputClasses,
				root: marginTop,
				disabled: classes.disabled,
				underline: underlineClasses
			},
			id: id
		}, inputProps)),
		error ? _react2.default.createElement(_Clear2.default, { className: feedbackClasses + ' ' + classes.labelRootError }) : success ? _react2.default.createElement(_Check2.default, { className: feedbackClasses + ' ' + classes.labelRootSuccess }) : null
	);
}

CustomInputComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	labelText: _propTypes2.default.node,
	labelProps: _propTypes2.default.object,
	id: _propTypes2.default.string,
	inputProps: _propTypes2.default.object,
	formControlProps: _propTypes2.default.object,
	inputRootCustomClasses: _propTypes2.default.string,
	error: _propTypes2.default.bool,
	success: _propTypes2.default.bool,
	white: _propTypes2.default.bool
};

var CustomInput = (0, _withStyles2.default)(_styles2.default)(CustomInputComponent);
exports.default = CustomInput;

//# sourceMappingURL=CustomInput.js.map