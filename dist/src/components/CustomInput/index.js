'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _CustomInput = require('./CustomInput');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_CustomInput).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//# sourceMappingURL=index.js.map