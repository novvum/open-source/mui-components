'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Card = require('../Card');

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _core = require('@material-ui/core');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Component(props) {
	return _react2.default.createElement(
		_Card.Card,
		null,
		_react2.default.createElement(
			_Card.CardHeader,
			{ color: props.color, stats: true, icon: true },
			_react2.default.createElement(
				CardIcon,
				{ color: props.color },
				props.headerIcon
			),
			_react2.default.createElement(
				'p',
				{ className: styles.cardCategory },
				props.category
			),
			_react2.default.createElement(
				'h3',
				{ className: styles.cardTitle },
				props.title
			)
		),
		_react2.default.createElement(
			_Card.CardFooter,
			{ stats: true },
			_react2.default.createElement(
				'div',
				{ className: styles.stats },
				props.statIcon && props.statIcon,
				props.stat
			)
		)
	);
}

var StatsCard = (0, _core.withStyles)(_styles2.default)(Component);
exports.default = StatsCard;

/**
 * @render react
 * @name StatsCard
 * @example
 * <div style={{padding: '25px'}}>
 * 	<StatsCard
 *    color="success"
 *    headerIcon={<h1>icon</h2>}
 *    category="Category"
 *    title="Title"
 *    statIcon={<p>stat</p>}
 *    stat="Stat"
 *  />
 * </div>
 */

//# sourceMappingURL=index.js.map