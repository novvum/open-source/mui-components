'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function BadgeComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    color = props.color,
	    children = props.children;

	return _react2.default.createElement(
		'span',
		{ className: classes.badge + ' ' + classes[color] },
		children
	);
}

BadgeComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	color: _propTypes2.default.oneOf(['primary', 'warning', 'danger', 'success', 'info', 'rose', 'gray'])
};

var Badge = (0, _withStyles2.default)(_styles2.default)(BadgeComponent);
exports.default = Badge;

/**
 * @render react
 * @name Badge
 * @example
 * <center>
 * <Badge color="primary">Primary</Badge>
 * <Badge color="warning">Warning</Badge>
 * <Badge color="danger">Danger</Badge>
 * <Badge color="success">Success</Badge>
 * <Badge color="info">Info</Badge>
 * <Badge color="rose">Rose</Badge>
 * <Badge color="gray">Gray</Badge>
 * </center>
 */

//# sourceMappingURL=Badge.js.map