'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var badgeStyle = {
	badge: {
		borderRadius: '12px',
		padding: '5px 12px',
		textTransform: 'uppercase',
		fontSize: '10px',
		fontWeight: '700',
		lineHeight: '1',
		color: '#fff',
		textAlign: 'center',
		verticalAlign: 'baseline',
		display: 'inline-block'
	},
	primary: {
		backgroundColor: _styles.primaryColor
	},
	warning: {
		backgroundColor: _styles.warningColor
	},
	danger: {
		backgroundColor: _styles.dangerColor
	},
	success: {
		backgroundColor: _styles.successColor
	},
	info: {
		backgroundColor: _styles.infoColor
	},
	rose: {
		backgroundColor: _styles.roseColor
	},
	gray: {
		backgroundColor: _styles.grayColor
	}
}; // ##############################
// // // Badge component styles
// #############################

exports.default = badgeStyle;

//# sourceMappingURL=styles.js.map