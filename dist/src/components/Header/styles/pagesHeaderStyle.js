'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../../styles');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // ##############################
// // // Pages Header styles
// #############################

var pagesHeaderStyle = function pagesHeaderStyle(theme) {
	return {
		appBar: {
			backgroundColor: 'transparent',
			boxShadow: 'none',
			borderBottom: '0',
			marginBottom: '0',
			position: 'absolute',
			width: '100%',
			paddingTop: '10px',
			zIndex: '1029',
			color: '#555555',
			border: '0',
			borderRadius: '3px',
			padding: '10px 0',
			transition: 'all 150ms ease 0s',
			minHeight: '50px',
			display: 'block'
		},
		container: Object.assign({}, _styles.container, {
			minHeight: '50px'
		}),
		flex: {
			flex: 1
		},
		title: Object.assign({}, _styles.defaultFont, {
			lineHeight: '30px',
			fontSize: '18px',
			borderRadius: '3px',
			textTransform: 'none',
			color: '#FFFFFF',
			'&:hover,&:focus': {
				background: 'transparent',
				color: '#FFFFFF'
			}
		}),
		appResponsive: {
			top: '8px'
		},
		primary: Object.assign({
			backgroundColor: _styles.primaryColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		info: Object.assign({
			backgroundColor: _styles.infoColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		success: Object.assign({
			backgroundColor: _styles.successColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		warning: Object.assign({
			backgroundColor: _styles.warningColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		danger: Object.assign({
			backgroundColor: _styles.dangerColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		list: Object.assign({}, _styles.defaultFont, {
			fontSize: '14px',
			margin: 0,
			marginRight: '-15px',
			paddingLeft: '0',
			listStyle: 'none',
			color: '#FFFFFF',
			paddingTop: '0',
			paddingBottom: '0'
		}),
		listItem: _defineProperty({
			float: 'left',
			position: 'relative',
			display: 'block',
			width: 'auto',
			margin: '0',
			padding: '0'
		}, theme.breakpoints.down('sm'), {
			zIndex: '999',
			width: '100%',
			paddingRight: '15px'
		}),
		navLink: {
			color: '#FFFFFF',
			margin: '0 5px',
			paddingTop: '15px',
			paddingBottom: '15px',
			fontWeight: '500',
			fontSize: '12px',
			textTransform: 'uppercase',
			borderRadius: '3px',
			lineHeight: '20px',
			position: 'relative',
			display: 'block',
			padding: '10px 15px',
			textDecoration: 'none',
			'&:hover,&:focus': {
				color: '#FFFFFF',
				background: 'rgba(200, 200, 200, 0.2)'
			}
		},
		listItemIcon: {
			marginTop: '-3px',
			top: '0px',
			position: 'relative',
			marginRight: '3px',
			width: '20px',
			height: '20px',
			verticalAlign: 'middle',
			color: 'inherit',
			display: 'inline-block'
		},
		listItemText: {
			flex: 'none',
			padding: '0',
			minWidth: '0',
			margin: 0,
			display: 'inline-block',
			position: 'relative',
			whiteSpace: 'nowrap'
		},
		navLinkActive: {
			backgroundColor: 'rgba(255, 255, 255, 0.1)'
		},
		drawerPaper: Object.assign({
			border: 'none',
			bottom: '0',
			transitionProperty: 'top, bottom, width',
			transitionDuration: '.2s, .2s, .35s',
			transitionTimingFunction: 'linear, linear, ease'
		}, _styles.boxShadow, {
			width: _styles.drawerWidth
		}, _styles.boxShadow, {
			position: 'fixed',
			display: 'block',
			top: '0',
			height: '100vh',
			right: '0',
			left: 'auto',
			visibility: 'visible',
			overflowY: 'visible',
			borderTop: 'none',
			textAlign: 'left',
			paddingRight: '0px',
			paddingLeft: '0'
		}, _styles.transition, {
			'&:before,&:after': {
				position: 'absolute',
				zIndex: '3',
				width: '100%',
				height: '100%',
				content: '""',
				display: 'block',
				top: '0'
			},
			'&:after': {
				background: '#000',
				opacity: '.8'
			}
		}),
		sidebarButton: {
			'&,&:hover,&:focus': {
				color: '#FFFFFF'
			},
			top: '-2px'
		}
	};
};

exports.default = pagesHeaderStyle;

//# sourceMappingURL=pagesHeaderStyle.js.map