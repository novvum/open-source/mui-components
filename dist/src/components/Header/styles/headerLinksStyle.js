'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../../styles');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // ##############################
// // // HeaderLinks styles
// #############################

var headerLinksStyle = function headerLinksStyle(theme) {
	var _searchRTL, _notifications;

	return {
		popperClose: {
			pointerEvents: 'none'
		},
		search: _defineProperty({}, theme.breakpoints.down('sm'), {
			margin: '10px 15px',
			float: 'none !important',
			paddingTop: '1px',
			paddingBottom: '1px',
			padding: '10px 15px',
			width: 'auto'
		}),
		searchInput: {
			paddingTop: '2px'
		},
		searchRTL: (_searchRTL = {}, _defineProperty(_searchRTL, theme.breakpoints.down('sm'), {
			marginRight: '18px !important'
		}), _defineProperty(_searchRTL, theme.breakpoints.up('md'), {
			marginLeft: '12px'
		}), _searchRTL),
		linkText: Object.assign({
			zIndex: '4'
		}, _styles.defaultFont, {
			fontSize: '14px',
			margin: '0!important',
			textTransform: 'none'
		}),
		buttonLink: _defineProperty({}, theme.breakpoints.down('sm'), {
			display: 'flex',
			margin: '5px 15px 0',
			width: 'auto',
			height: 'auto',
			'& svg': {
				width: '30px',
				height: '24px',
				marginRight: '19px',
				marginLeft: '3px'
			}
		}),
		searchButton: _defineProperty({}, theme.breakpoints.down('sm'), {
			top: '-50px !important',
			marginRight: '38px',
			float: 'right'
		}),
		top: {
			zIndex: '4'
		},
		searchIcon: {
			width: '17px',
			zIndex: '4'
		},
		links: _defineProperty({
			width: '20px',
			height: '20px',
			zIndex: '4'
		}, theme.breakpoints.down('sm'), {
			display: 'block',
			width: '30px',
			height: '30px',
			color: 'inherit',
			opacity: '0.8',
			marginRight: '16px',
			marginLeft: '-5px'
		}),
		notifications: (_notifications = {
			zIndex: '4'
		}, _defineProperty(_notifications, theme.breakpoints.up('md'), {
			position: 'absolute',
			top: '5px',
			border: '1px solid #FFF',
			right: '5px',
			fontSize: '9px',
			background: _styles.dangerColor,
			color: '#FFFFFF',
			minWidth: '16px',
			height: '16px',
			borderRadius: '10px',
			textAlign: 'center',
			lineHeight: '14px',
			verticalAlign: 'middle',
			display: 'block'
		}), _defineProperty(_notifications, theme.breakpoints.down('sm'), Object.assign({}, _styles.defaultFont, {
			fontSize: '14px',
			marginRight: '8px'
		})), _notifications),
		dropdown: {
			borderRadius: '3px',
			border: '0',
			boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.26)',
			top: '100%',
			zIndex: '1000',
			minWidth: '160px',
			padding: '5px 0',
			margin: '2px 0 0',
			fontSize: '14px',
			textAlign: 'left',
			listStyle: 'none',
			backgroundColor: '#fff',
			backgroundClip: 'padding-box'
		},
		pooperResponsive: _defineProperty({}, theme.breakpoints.down('sm'), {
			zIndex: '1640',
			position: 'static',
			float: 'none',
			width: 'auto',
			marginTop: '0',
			backgroundColor: 'transparent',
			border: '0',
			boxShadow: 'none',
			color: 'black'
		}),
		dropdownItem: Object.assign({}, _styles.defaultFont, {
			fontSize: '13px',
			padding: '10px 20px',
			margin: '0 5px',
			borderRadius: '2px',
			position: 'relative',
			transition: 'all 150ms linear',
			display: 'block',
			clear: 'both',
			fontWeight: '400',
			height: 'fit-content',
			color: '#333',
			whiteSpace: 'nowrap',
			'&:hover': Object.assign({
				backgroundColor: _styles.primaryColor,
				color: '#FFFFFF'
			}, _styles.primaryBoxShadow)
		}),
		dropdownItemRTL: {
			textAlign: 'right !important'
		},
		wrapperRTL: _defineProperty({}, theme.breakpoints.up('md'), {
			paddingLeft: '16px'
		}),
		buttonLinkRTL: _defineProperty({}, theme.breakpoints.down('sm'), {
			alignItems: 'center',
			justifyContent: 'flex-end',
			width: '-webkit-fill-available',
			margin: '10px 15px 0',
			padding: '10px 15px',
			display: 'block',
			position: 'relative'
		}),
		labelRTL: _defineProperty({}, theme.breakpoints.down('sm'), {
			flexDirection: 'row-reverse',
			justifyContent: 'initial',
			display: 'flex'
		}),
		linksRTL: _defineProperty({}, theme.breakpoints.down('sm'), {
			marginRight: '-5px !important',
			marginLeft: '16px !important'
		}),
		managerClasses: _defineProperty({}, theme.breakpoints.up('md'), {
			display: 'inline-block'
		}),
		headerLinksSvg: {
			width: '20px !important',
			height: '20px !important'
		}
	};
};

exports.default = headerLinksStyle;

//# sourceMappingURL=headerLinksStyle.js.map