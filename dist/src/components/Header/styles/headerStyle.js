'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../../styles');

var headerStyle = function headerStyle(theme) {
	return {
		appBar: {
			backgroundColor: 'transparent',
			boxShadow: 'none',
			borderBottom: '0',
			marginBottom: '0',
			position: 'absolute',
			width: '100%',
			paddingTop: '10px',
			zIndex: '1029',
			color: '#555555',
			border: '0',
			borderRadius: '3px',
			padding: '10px 0',
			transition: 'all 150ms ease 0s',
			minHeight: '50px',
			display: 'block'
		},
		container: Object.assign({}, _styles.containerFluid, {
			minHeight: '50px'
		}),
		flex: {
			flex: 1
		},
		title: Object.assign({}, _styles.defaultFont, {
			lineHeight: '30px',
			fontSize: '18px',
			borderRadius: '3px',
			textTransform: 'none',
			color: 'inherit',
			paddingTop: '0.625rem',
			paddingBottom: '0.625rem',
			'&:hover,&:focus': {
				background: 'transparent'
			}
		}),
		primary: Object.assign({
			backgroundColor: _styles.primaryColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		info: Object.assign({
			backgroundColor: _styles.infoColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		success: Object.assign({
			backgroundColor: _styles.successColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		warning: Object.assign({
			backgroundColor: _styles.warningColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		danger: Object.assign({
			backgroundColor: _styles.dangerColor,
			color: '#FFFFFF'
		}, _styles.defaultBoxShadow),
		sidebarMinimize: {
			float: 'left',
			padding: '0 0 0 15px',
			display: 'block',
			color: '#555555'
		},
		sidebarMinimizeRTL: {
			padding: '0 15px 0 0 !important'
		},
		sidebarMiniIcon: {
			width: '20px',
			height: '17px'
		}
	};
}; // ##############################
// // // Header styles
// #############################

exports.default = headerStyle;

//# sourceMappingURL=headerStyle.js.map