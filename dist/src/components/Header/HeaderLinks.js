'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactPopper = require('react-popper');

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _MenuList = require('@material-ui/core/MenuList');

var _MenuList2 = _interopRequireDefault(_MenuList);

var _ClickAwayListener = require('@material-ui/core/ClickAwayListener');

var _ClickAwayListener2 = _interopRequireDefault(_ClickAwayListener);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _Grow = require('@material-ui/core/Grow');

var _Grow2 = _interopRequireDefault(_Grow);

var _Hidden = require('@material-ui/core/Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _Person = require('@material-ui/icons/Person');

var _Person2 = _interopRequireDefault(_Person);

var _Notifications = require('@material-ui/icons/Notifications');

var _Notifications2 = _interopRequireDefault(_Notifications);

var _Dashboard = require('@material-ui/icons/Dashboard');

var _Dashboard2 = _interopRequireDefault(_Dashboard);

var _Search = require('@material-ui/icons/Search');

var _Search2 = _interopRequireDefault(_Search);

var _CustomInput = require('../CustomInput');

var _CustomInput2 = _interopRequireDefault(_CustomInput);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _headerLinksStyle = require('./styles/headerLinksStyle');

var _headerLinksStyle2 = _interopRequireDefault(_headerLinksStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// @material-ui/core components


// @material-ui/icons


// core components


var HeaderLinksComponent = function (_React$Component) {
	_inherits(HeaderLinksComponent, _React$Component);

	function HeaderLinksComponent() {
		var _ref;

		var _temp, _this, _ret;

		_classCallCheck(this, HeaderLinksComponent);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = HeaderLinksComponent.__proto__ || Object.getPrototypeOf(HeaderLinksComponent)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
			open: false
		}, _this.handleClick = function () {
			_this.setState({ open: !_this.state.open });
		}, _this.handleClose = function () {
			_this.setState({ open: false });
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(HeaderLinksComponent, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    classes = _props.classes,
			    rtlActive = _props.rtlActive;
			var open = this.state.open;

			var searchButton = classes.top + ' ' + classes.searchButton + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.searchRTL, rtlActive));
			var dropdownItem = classes.dropdownItem + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.dropdownItemRTL, rtlActive));
			var wrapper = (0, _classnames2.default)(_defineProperty({}, classes.wrapperRTL, rtlActive));
			var managerClasses = (0, _classnames2.default)(_defineProperty({}, classes.managerClasses, true));
			return _react2.default.createElement(
				'div',
				{ className: wrapper },
				_react2.default.createElement(_CustomInput2.default, {
					rtlActive: rtlActive,
					formControlProps: {
						className: classes.top + ' ' + classes.search
					},
					inputProps: {
						placeholder: rtlActive ? 'بحث' : 'Search',
						inputProps: {
							'aria-label': rtlActive ? 'بحث' : 'Search',
							className: classes.searchInput
						}
					}
				}),
				_react2.default.createElement(
					_CustomButtons2.default,
					{
						color: 'white',
						'aria-label': 'edit',
						justIcon: true,
						round: true,
						className: searchButton
					},
					_react2.default.createElement(_Search2.default, {
						className: classes.headerLinksSvg + ' ' + classes.searchIcon
					})
				),
				_react2.default.createElement(
					_CustomButtons2.default,
					{
						color: 'transparent',
						simple: true,
						'aria-label': 'Dashboard',
						justIcon: true,
						className: rtlActive ? classes.buttonLinkRTL : classes.buttonLink,
						muiClasses: {
							label: rtlActive ? classes.labelRTL : ''
						}
					},
					_react2.default.createElement(_Dashboard2.default, {
						className: classes.headerLinksSvg + ' ' + (rtlActive ? classes.links + ' ' + classes.linksRTL : classes.links)
					}),
					_react2.default.createElement(
						_Hidden2.default,
						{ mdUp: true },
						_react2.default.createElement(
							'span',
							{ className: classes.linkText },
							rtlActive ? 'لوحة القيادة' : 'Dashboard'
						)
					)
				),
				_react2.default.createElement(
					_reactPopper.Manager,
					{ className: managerClasses },
					_react2.default.createElement(
						_reactPopper.Target,
						null,
						_react2.default.createElement(
							_CustomButtons2.default,
							{
								color: 'transparent',
								justIcon: true,
								'aria-label': 'Notifications',
								'aria-owns': open ? 'menu-list' : null,
								'aria-haspopup': 'true',
								onClick: this.handleClick,
								className: rtlActive ? classes.buttonLinkRTL : classes.buttonLink,
								muiClasses: {
									label: rtlActive ? classes.labelRTL : ''
								}
							},
							_react2.default.createElement(_Notifications2.default, {
								className: classes.headerLinksSvg + ' ' + (rtlActive ? classes.links + ' ' + classes.linksRTL : classes.links)
							}),
							_react2.default.createElement(
								'span',
								{ className: classes.notifications },
								'5'
							),
							_react2.default.createElement(
								_Hidden2.default,
								{ mdUp: true },
								_react2.default.createElement(
									'span',
									{ onClick: this.handleClick, className: classes.linkText },
									rtlActive ? 'إعلام' : 'Notification'
								)
							)
						)
					),
					_react2.default.createElement(
						_reactPopper.Popper,
						{
							placement: 'bottom-start',
							eventsEnabled: open,
							className: (0, _classnames2.default)(_defineProperty({}, classes.popperClose, !open)) + ' ' + classes.pooperResponsive
						},
						_react2.default.createElement(
							_ClickAwayListener2.default,
							{ onClickAway: this.handleClose },
							_react2.default.createElement(
								_Grow2.default,
								{
									'in': open,
									id: 'menu-list',
									style: { transformOrigin: '0 0 0' }
								},
								_react2.default.createElement(
									_Paper2.default,
									{ className: classes.dropdown },
									_react2.default.createElement(
										_MenuList2.default,
										{ role: 'menu' },
										_react2.default.createElement(
											_MenuItem2.default,
											{
												onClick: this.handleClose,
												className: dropdownItem
											},
											rtlActive ? 'إجلاء أوزار الأسيوي حين بل, كما' : 'Mike John responded to your email'
										),
										_react2.default.createElement(
											_MenuItem2.default,
											{
												onClick: this.handleClose,
												className: dropdownItem
											},
											rtlActive ? 'شعار إعلان الأرضية قد ذلك' : 'You have 5 new tasks'
										),
										_react2.default.createElement(
											_MenuItem2.default,
											{
												onClick: this.handleClose,
												className: dropdownItem
											},
											rtlActive ? 'ثمّة الخاصّة و على. مع جيما' : "You're now friend with Andrew"
										),
										_react2.default.createElement(
											_MenuItem2.default,
											{
												onClick: this.handleClose,
												className: dropdownItem
											},
											rtlActive ? 'قد علاقة' : 'Another Notification'
										),
										_react2.default.createElement(
											_MenuItem2.default,
											{
												onClick: this.handleClose,
												className: dropdownItem
											},
											rtlActive ? 'قد فاتّبع' : 'Another One'
										)
									)
								)
							)
						)
					)
				),
				_react2.default.createElement(
					_CustomButtons2.default,
					{
						color: 'transparent',
						'aria-label': 'Person',
						justIcon: true,
						className: rtlActive ? classes.buttonLinkRTL : classes.buttonLink,
						muiClasses: {
							label: rtlActive ? classes.labelRTL : ''
						}
					},
					_react2.default.createElement(_Person2.default, {
						className: classes.headerLinksSvg + ' ' + (rtlActive ? classes.links + ' ' + classes.linksRTL : classes.links)
					}),
					_react2.default.createElement(
						_Hidden2.default,
						{ mdUp: true },
						_react2.default.createElement(
							'span',
							{ className: classes.linkText },
							rtlActive ? 'الملف الشخصي' : 'Profile'
						)
					)
				)
			);
		}
	}]);

	return HeaderLinksComponent;
}(_react2.default.Component);

HeaderLinksComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	rtlActive: _propTypes2.default.bool
};

var HeaderLinks = (0, _withStyles2.default)(_headerLinksStyle2.default)(HeaderLinksComponent);
exports.default = HeaderLinks;

//# sourceMappingURL=HeaderLinks.js.map