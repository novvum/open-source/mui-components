'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouterDom = require('react-router-dom');

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = require('@material-ui/core/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Toolbar = require('@material-ui/core/Toolbar');

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Hidden = require('@material-ui/core/Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _Drawer = require('@material-ui/core/Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _List = require('@material-ui/core/List');

var _List2 = _interopRequireDefault(_List);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _ListItemIcon = require('@material-ui/core/ListItemIcon');

var _ListItemIcon2 = _interopRequireDefault(_ListItemIcon);

var _ListItemText = require('@material-ui/core/ListItemText');

var _ListItemText2 = _interopRequireDefault(_ListItemText);

var _Dashboard = require('@material-ui/icons/Dashboard');

var _Dashboard2 = _interopRequireDefault(_Dashboard);

var _Menu = require('@material-ui/icons/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _pagesHeaderStyle = require('./styles/pagesHeaderStyle');

var _pagesHeaderStyle2 = _interopRequireDefault(_pagesHeaderStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// @material-ui/core components


// @material-ui/icons


// core components


var PagesHeaderComponent = function (_React$Component) {
	_inherits(PagesHeaderComponent, _React$Component);

	function PagesHeaderComponent(props) {
		_classCallCheck(this, PagesHeaderComponent);

		var _this = _possibleConstructorReturn(this, (PagesHeaderComponent.__proto__ || Object.getPrototypeOf(PagesHeaderComponent)).call(this, props));

		_this.handleDrawerToggle = function () {
			_this.setState({ open: !_this.state.open });
		};

		_this.state = {
			open: false
		};
		return _this;
	}

	_createClass(PagesHeaderComponent, [{
		key: 'activeRoute',

		// verifies if routeName is the one active (in browser input)
		value: function activeRoute(routeName) {
			return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
		}
	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(e) {
			if (e.history.location.pathname !== e.location.pathname) {
				this.setState({ open: false });
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props = this.props,
			    classes = _props.classes,
			    color = _props.color,
			    routes = _props.routes,
			    primaryRoute = _props.primaryRoute;

			var appBarClasses = (0, _classnames2.default)(_defineProperty({}, ' ' + classes[color], color));
			var list = _react2.default.createElement(
				_List2.default,
				{ className: classes.list },
				_react2.default.createElement(
					_ListItem2.default,
					{ className: classes.listItem },
					_react2.default.createElement(
						_reactRouterDom.NavLink,
						{ to: primaryRoute, className: classes.navLink },
						_react2.default.createElement(
							_ListItemIcon2.default,
							{ className: classes.listItemIcon },
							_react2.default.createElement(_Dashboard2.default, null)
						),
						_react2.default.createElement(_ListItemText2.default, {
							primary: 'Dashboard',
							disableTypography: true,
							className: classes.listItemText
						})
					)
				),
				routes.map(function (prop, key) {
					if (prop.redirect) {
						return null;
					}
					var navLink = classes.navLink + (0, _classnames2.default)(_defineProperty({}, ' ' + classes.navLinkActive, _this2.activeRoute(prop.path)));
					return _react2.default.createElement(
						_ListItem2.default,
						{ key: key, className: classes.listItem },
						_react2.default.createElement(
							_reactRouterDom.NavLink,
							{ to: prop.path, className: navLink },
							_react2.default.createElement(
								_ListItemIcon2.default,
								{ className: classes.listItemIcon },
								_react2.default.createElement(prop.icon, null)
							),
							_react2.default.createElement(_ListItemText2.default, {
								primary: prop.short,
								disableTypography: true,
								className: classes.listItemText
							})
						)
					);
				})
			);
			return _react2.default.createElement(
				_AppBar2.default,
				{ position: 'static', className: classes.appBar + appBarClasses },
				_react2.default.createElement(
					_Toolbar2.default,
					{ className: classes.container },
					_react2.default.createElement(
						_Hidden2.default,
						{ smDown: true, implementation: 'css' },
						_react2.default.createElement(
							'div',
							{ className: classes.flex },
							_react2.default.createElement(
								_CustomButtons2.default,
								{ href: '#', className: classes.title, color: 'transparent' },
								'Material Dashboard Pro React'
							)
						)
					),
					_react2.default.createElement(
						_Hidden2.default,
						{ mdUp: true },
						_react2.default.createElement(
							'div',
							{ className: classes.flex },
							_react2.default.createElement(
								_CustomButtons2.default,
								{ href: '#', className: classes.title, color: 'transparent' },
								'MD Pro React'
							)
						)
					),
					_react2.default.createElement(
						_Hidden2.default,
						{ smDown: true, implementation: 'css' },
						list
					),
					_react2.default.createElement(
						_Hidden2.default,
						{ mdUp: true },
						_react2.default.createElement(
							_CustomButtons2.default,
							{
								className: classes.sidebarButton,
								color: 'transparent',
								justIcon: true,
								'aria-label': 'open drawer',
								onClick: this.handleDrawerToggle
							},
							_react2.default.createElement(_Menu2.default, null)
						)
					),
					_react2.default.createElement(
						_Hidden2.default,
						{ mdUp: true, implementation: 'css' },
						_react2.default.createElement(
							_Hidden2.default,
							{ mdUp: true },
							_react2.default.createElement(
								_Drawer2.default,
								{
									variant: 'temporary',
									anchor: 'right',
									open: this.state.open,
									classes: {
										paper: classes.drawerPaper
									},
									onClose: this.handleDrawerToggle,
									ModalProps: {
										keepMounted: true // Better open performance on mobile.
									}
								},
								list
							)
						)
					)
				)
			);
		}
	}]);

	return PagesHeaderComponent;
}(_react2.default.Component);

PagesHeaderComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	color: _propTypes2.default.oneOf(['primary', 'info', 'success', 'warning', 'danger']),
	routes: _propTypes2.default.array.isRequired,
	primaryRoute: _propTypes2.default.string
};

PagesHeaderComponent.defaultProps = {
	routes: [],
	primaryRoute: '/'
};

var PagesHeader = (0, _withStyles2.default)(_pagesHeaderStyle2.default)(PagesHeaderComponent);
exports.default = PagesHeader;

//# sourceMappingURL=PagesHeader.js.map