'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = require('@material-ui/core/AppBar');

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Toolbar = require('@material-ui/core/Toolbar');

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Hidden = require('@material-ui/core/Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _Menu = require('@material-ui/icons/Menu');

var _Menu2 = _interopRequireDefault(_Menu);

var _MoreVert = require('@material-ui/icons/MoreVert');

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _ViewList = require('@material-ui/icons/ViewList');

var _ViewList2 = _interopRequireDefault(_ViewList);

var _HeaderLinks = require('./HeaderLinks');

var _HeaderLinks2 = _interopRequireDefault(_HeaderLinks);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _headerStyle = require('./styles/headerStyle');

var _headerStyle2 = _interopRequireDefault(_headerStyle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


// material-ui icons


// core components


function HeaderComponent(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	function makeBrand() {
		var name;
		props.routes.map(function (prop, key) {
			if (prop.collapse) {
				prop.views.map(function (prop, key) {
					if (prop.path === props.location.pathname) {
						name = prop.name;
					}
					return null;
				});
			}
			if (prop.path === props.location.pathname) {
				name = prop.name;
			}
			return null;
		});
		return name;
	}
	var classes = props.classes,
	    color = props.color,
	    rtlActive = props.rtlActive;

	var appBarClasses = (0, _classnames2.default)(_defineProperty({}, ' ' + classes[color], color));
	var sidebarMinimize = classes.sidebarMinimize + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.sidebarMinimizeRTL, rtlActive));
	return _react2.default.createElement(
		_AppBar2.default,
		{ className: classes.appBar + appBarClasses },
		_react2.default.createElement(
			_Toolbar2.default,
			{ className: classes.container },
			_react2.default.createElement(
				_Hidden2.default,
				{ smDown: true },
				_react2.default.createElement(
					'div',
					{ className: sidebarMinimize },
					props.miniActive ? _react2.default.createElement(
						_CustomButtons2.default,
						{
							justIcon: true,
							round: true,
							color: 'white',
							onClick: props.sidebarMinimize
						},
						_react2.default.createElement(_ViewList2.default, { className: classes.sidebarMiniIcon })
					) : _react2.default.createElement(
						_CustomButtons2.default,
						{
							justIcon: true,
							round: true,
							color: 'white',
							onClick: props.sidebarMinimize
						},
						_react2.default.createElement(_MoreVert2.default, { className: classes.sidebarMiniIcon })
					)
				)
			),
			_react2.default.createElement(
				'div',
				{ className: classes.flex },
				_react2.default.createElement(
					_CustomButtons2.default,
					{ href: '#', className: classes.title, color: 'transparent' },
					makeBrand()
				)
			),
			_react2.default.createElement(
				_Hidden2.default,
				{ smDown: true, implementation: 'css' },
				_react2.default.createElement(_HeaderLinks2.default, { rtlActive: rtlActive })
			),
			_react2.default.createElement(
				_Hidden2.default,
				{ mdUp: true },
				_react2.default.createElement(
					_CustomButtons2.default,
					{
						className: classes.appResponsive,
						color: 'transparent',
						justIcon: true,
						'aria-label': 'open drawer',
						onClick: props.handleDrawerToggle
					},
					_react2.default.createElement(_Menu2.default, null)
				)
			)
		)
	);
}

HeaderComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	color: _propTypes2.default.oneOf(['primary', 'info', 'success', 'warning', 'danger']),
	rtlActive: _propTypes2.default.bool
};

var Header = (0, _withStyles2.default)(_headerStyle2.default)(HeaderComponent);
exports.default = Header;

//# sourceMappingURL=Header.js.map