'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _List = require('@material-ui/core/List');

var _List2 = _interopRequireDefault(_List);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function FooterComponent(_ref) {
	var _cx, _cx3;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    fluid = props.fluid,
	    white = props.white,
	    rtlActive = props.rtlActive;

	var container = (0, _classnames2.default)((_cx = {}, _defineProperty(_cx, classes.container, !fluid), _defineProperty(_cx, classes.containerFluid, fluid), _defineProperty(_cx, classes.whiteColor, white), _cx));
	var anchor = classes.a + (0, _classnames2.default)(_defineProperty({}, ' ' + classes.whiteColor, white));
	var block = (0, _classnames2.default)((_cx3 = {}, _defineProperty(_cx3, classes.block, true), _defineProperty(_cx3, classes.whiteColor, white), _cx3));
	return _react2.default.createElement(
		'footer',
		{ className: classes.footer },
		_react2.default.createElement(
			'div',
			{ className: container },
			_react2.default.createElement(
				'div',
				{ className: classes.left },
				_react2.default.createElement(
					_List2.default,
					{ className: classes.list },
					_react2.default.createElement(
						_ListItem2.default,
						{ className: classes.inlineBlock },
						_react2.default.createElement(
							'a',
							{ href: '#home', className: block },
							rtlActive ? 'الصفحة الرئيسية' : 'Home'
						)
					),
					_react2.default.createElement(
						_ListItem2.default,
						{ className: classes.inlineBlock },
						_react2.default.createElement(
							'a',
							{ href: '#company', className: block },
							rtlActive ? 'شركة' : 'Company'
						)
					),
					_react2.default.createElement(
						_ListItem2.default,
						{ className: classes.inlineBlock },
						_react2.default.createElement(
							'a',
							{ href: '#portfolio', className: block },
							rtlActive ? 'بعدسة' : 'Portfolio'
						)
					),
					_react2.default.createElement(
						_ListItem2.default,
						{ className: classes.inlineBlock },
						_react2.default.createElement(
							'a',
							{ href: '#blog', className: block },
							rtlActive ? 'مدونة' : 'Blog'
						)
					)
				)
			),
			_react2.default.createElement(
				'p',
				{ className: classes.right },
				'\xA9 ',
				1900 + new Date().getYear(),
				' ',
				_react2.default.createElement(
					'a',
					{ href: 'https://www.creative-tim.com', className: anchor },
					rtlActive ? 'توقيت الإبداعية' : 'Creative Tim'
				),
				rtlActive ? ', مصنوعة مع الحب لشبكة الإنترنت أفضل' : ', made with love for a better web'
			)
		)
	);
}

FooterComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	fluid: _propTypes2.default.bool,
	white: _propTypes2.default.bool,
	rtlActive: _propTypes2.default.bool
};

var Footer = (0, _withStyles2.default)(_styles2.default)(FooterComponent);
exports.default = Footer;

//# sourceMappingURL=Footer.js.map