'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var footerStyle = {
	block: {},
	left: {
		float: 'left!important',
		display: 'block'
	},
	right: {
		margin: '0',
		fontSize: '14px',
		float: 'right!important',
		padding: '15px'
	},
	footer: Object.assign({
		bottom: '0',
		borderTop: '1px solid #e7e7e7',
		padding: '15px 0'
	}, _styles.defaultFont, {
		zIndex: 4
	}),
	container: Object.assign({
		zIndex: 3
	}, _styles.container, {
		position: 'relative'
	}),
	containerFluid: Object.assign({
		zIndex: 3
	}, _styles.containerFluid, {
		position: 'relative'
	}),
	a: {
		color: _styles.primaryColor,
		textDecoration: 'none',
		backgroundColor: 'transparent'
	},
	list: {
		marginBottom: '0',
		padding: '0',
		marginTop: '0'
	},
	inlineBlock: {
		display: 'inline-block',
		padding: '0',
		width: 'auto'
	},
	whiteColor: {
		'&,&:hover,&:focus': {
			color: '#FFFFFF'
		}
	}
}; // ##############################
// // // Footer styles
// #############################

exports.default = footerStyle;

//# sourceMappingURL=styles.js.map