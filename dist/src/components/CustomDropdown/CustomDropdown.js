'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactPopper = require('react-popper');

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _MenuItem = require('@material-ui/core/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _MenuList = require('@material-ui/core/MenuList');

var _MenuList2 = _interopRequireDefault(_MenuList);

var _ClickAwayListener = require('@material-ui/core/ClickAwayListener');

var _ClickAwayListener2 = _interopRequireDefault(_ClickAwayListener);

var _Paper = require('@material-ui/core/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _Grow = require('@material-ui/core/Grow');

var _Grow2 = _interopRequireDefault(_Grow);

var _Divider = require('@material-ui/core/Divider');

var _Divider2 = _interopRequireDefault(_Divider);

var _CustomButtons = require('../CustomButtons');

var _CustomButtons2 = _interopRequireDefault(_CustomButtons);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// nodejs library that concatenates classes

// nodejs library to set properties for components


// material-ui components


// core components


var DropdownComponent = function (_React$Component) {
	_inherits(DropdownComponent, _React$Component);

	function DropdownComponent(props) {
		_classCallCheck(this, DropdownComponent);

		var _this = _possibleConstructorReturn(this, (DropdownComponent.__proto__ || Object.getPrototypeOf(DropdownComponent)).call(this, props));

		_this.state = {
			open: false
		};
		_this.handleClick = _this.handleClick.bind(_this);
		_this.handleClose = _this.handleClose.bind(_this);
		return _this;
	}

	_createClass(DropdownComponent, [{
		key: 'handleClick',
		value: function handleClick() {
			this.setState({ open: true });
		}
	}, {
		key: 'handleClose',
		value: function handleClose() {
			this.setState({ open: false });
		}
	}, {
		key: 'render',
		value: function render() {
			var _classNames,
			    _classNames2,
			    _classNames3,
			    _this2 = this;

			var open = this.state.open;
			var _props = this.props,
			    classes = _props.classes,
			    buttonText = _props.buttonText,
			    buttonIcon = _props.buttonIcon,
			    dropdownList = _props.dropdownList,
			    buttonProps = _props.buttonProps,
			    dropup = _props.dropup,
			    dropdownHeader = _props.dropdownHeader,
			    caret = _props.caret,
			    hoverColor = _props.hoverColor,
			    left = _props.left,
			    rtlActive = _props.rtlActive;

			var caretClasses = (0, _classnames2.default)((_classNames = {}, _defineProperty(_classNames, classes.caret, true), _defineProperty(_classNames, classes.caretDropup, dropup && !open), _defineProperty(_classNames, classes.caretActive, open && !dropup), _defineProperty(_classNames, classes.caretRTL, rtlActive), _classNames));
			var dropdownItem = (0, _classnames2.default)((_classNames2 = {}, _defineProperty(_classNames2, classes.dropdownItem, true), _defineProperty(_classNames2, classes[hoverColor + 'Hover'], true), _defineProperty(_classNames2, classes.dropdownItemRTL, rtlActive), _classNames2));
			return _react2.default.createElement(
				_reactPopper.Manager,
				null,
				_react2.default.createElement(
					_reactPopper.Target,
					null,
					_react2.default.createElement(
						_CustomButtons2.default,
						Object.assign({
							'aria-label': 'Notifications',
							'aria-owns': open ? 'menu-list' : null,
							'aria-haspopup': 'true'
						}, buttonProps, {
							onClick: this.handleClick
						}),
						buttonIcon !== undefined ? _react2.default.createElement(this.props.buttonIcon, { className: classes.buttonIcon }) : null,
						buttonText !== undefined ? buttonText : null,
						caret ? _react2.default.createElement('b', { className: caretClasses }) : null
					)
				),
				_react2.default.createElement(
					_reactPopper.Popper,
					{
						placement: dropup ? left ? 'top-end' : 'top-start' : left ? 'bottom-end' : 'bottom-start',
						eventsEnabled: open,
						className: (0, _classnames2.default)((_classNames3 = {}, _defineProperty(_classNames3, classes.popperClose, !open), _defineProperty(_classNames3, classes.pooperResponsive, true), _classNames3))
					},
					_react2.default.createElement(
						_ClickAwayListener2.default,
						{ onClickAway: this.handleClose },
						_react2.default.createElement(
							_Grow2.default,
							{
								'in': open,
								id: 'menu-list',
								style: dropup ? { transformOrigin: '0 100% 0' } : { transformOrigin: '0 0 0' }
							},
							_react2.default.createElement(
								_Paper2.default,
								{ className: classes.dropdown },
								_react2.default.createElement(
									_MenuList2.default,
									{ role: 'menu', className: classes.menuList },
									dropdownHeader !== undefined ? _react2.default.createElement(
										_MenuItem2.default,
										{
											onClick: this.handleClose,
											className: classes.dropdownHeader
										},
										dropdownHeader
									) : null,
									dropdownList.map(function (prop, key) {
										if (prop.divider) {
											return _react2.default.createElement(_Divider2.default, {
												key: key,
												onClick: _this2.handleClose,
												className: classes.dropdownDividerItem
											});
										}
										return _react2.default.createElement(
											_MenuItem2.default,
											{
												key: key,
												onClick: _this2.handleClose,
												className: dropdownItem
											},
											prop
										);
									})
								)
							)
						)
					)
				)
			);
		}
	}]);

	return DropdownComponent;
}(_react2.default.Component);

DropdownComponent.defaultProps = {
	caret: true,
	hoverColor: 'primary'
};

DropdownComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	hoverColor: _propTypes2.default.oneOf(['primary', 'black']),
	buttonText: _propTypes2.default.node,
	buttonIcon: _propTypes2.default.func,
	dropdownList: _propTypes2.default.array,
	buttonProps: _propTypes2.default.object,
	dropup: _propTypes2.default.bool,
	dropdownHeader: _propTypes2.default.node,
	rtlActive: _propTypes2.default.bool,
	caret: _propTypes2.default.bool,
	left: _propTypes2.default.bool
};

var CustomDropdown = (0, _withStyles2.default)(_styles2.default)(DropdownComponent);
exports.default = CustomDropdown;

//# sourceMappingURL=CustomDropdown.js.map