'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _GridContainer = require('../Grid/GridContainer');

var _GridContainer2 = _interopRequireDefault(_GridContainer);

var _GridItem = require('../Grid/GridItem');

var _GridItem2 = _interopRequireDefault(_GridItem);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


// core components


function InstructionComponent(_ref) {
	var _cx, _cx2;

	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    title = props.title,
	    text = props.text,
	    image = props.image,
	    className = props.className,
	    imageClassName = props.imageClassName,
	    imageAlt = props.imageAlt;

	var instructionClasses = (0, _classnames2.default)((_cx = {}, _defineProperty(_cx, classes.instruction, true), _defineProperty(_cx, className, className !== undefined), _cx));
	var pictureClasses = (0, _classnames2.default)((_cx2 = {}, _defineProperty(_cx2, classes.picture, true), _defineProperty(_cx2, imageClassName, imageClassName !== undefined), _cx2));
	return _react2.default.createElement(
		'div',
		{ className: instructionClasses },
		_react2.default.createElement(
			_GridContainer2.default,
			null,
			_react2.default.createElement(
				_GridItem2.default,
				{ xs: 12, sm: 12, md: 8 },
				_react2.default.createElement(
					'strong',
					null,
					title
				),
				_react2.default.createElement(
					'p',
					null,
					text
				)
			),
			_react2.default.createElement(
				_GridItem2.default,
				{ xs: 12, sm: 12, md: 4 },
				_react2.default.createElement(
					'div',
					{ className: pictureClasses },
					_react2.default.createElement('img', { src: image, alt: imageAlt, className: classes.image })
				)
			)
		)
	);
}

InstructionComponent.defaultProps = {
	imageAlt: '...'
};

InstructionComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	title: _propTypes2.default.node.isRequired,
	text: _propTypes2.default.node.isRequired,
	image: _propTypes2.default.string.isRequired,
	imageAlt: _propTypes2.default.string,
	className: _propTypes2.default.string,
	imageClassName: _propTypes2.default.string
};

var Instruction = (0, _withStyles2.default)(_styles2.default)(InstructionComponent);
exports.default = Instruction;

//# sourceMappingURL=Instruction.js.map