'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Instruction = require('./Instruction');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Instruction).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//# sourceMappingURL=index.js.map