'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
var instructionStyle = {
	instruction: {},
	picture: {},
	image: {
		width: '100%',
		height: 'auto',
		borderRadius: '6px',
		display: 'block',
		maxWidth: '100%'
	}
};

exports.default = instructionStyle;

//# sourceMappingURL=styles.js.map