'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

// @material-ui/core components


function SuccessText(_ref) {
	var props = _objectWithoutProperties(_ref, []);

	var classes = props.classes,
	    children = props.children;

	return _react2.default.createElement(
		'div',
		{ className: classes.defaultFontStyle + ' ' + classes.successText },
		children
	);
}

SuccessText.propTypes = {
	classes: _propTypes2.default.object.isRequired
};

var Success = (0, _withStyles2.default)(_styles2.default)(SuccessText);
exports.default = Success;

//# sourceMappingURL=Success.js.map