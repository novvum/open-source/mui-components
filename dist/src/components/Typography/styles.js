'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _styles = require('../styles');

var typographyStyle = {
	defaultFontStyle: Object.assign({}, _styles.defaultFont, {
		fontSize: '14px'
	}),
	defaultHeaderMargins: {
		marginTop: '20px',
		marginBottom: '10px'
	},
	quote: {
		padding: '10px 20px',
		margin: '0 0 20px',
		fontSize: '17.5px',
		borderLeft: '5px solid #eee'
	},
	quoteText: {
		margin: '0 0 10px',
		fontStyle: 'italic'
	},
	quoteAuthor: {
		display: 'block',
		fontSize: '80%',
		lineHeight: '1.42857143',
		color: '#777'
	},
	mutedText: {
		color: '#777'
	},
	primaryText: {
		color: _styles.primaryColor
	},
	infoText: {
		color: _styles.infoColor
	},
	successText: {
		color: _styles.successColor
	},
	warningText: {
		color: _styles.warningColor
	},
	dangerText: {
		color: _styles.dangerColor
	}
}; // ##############################
// // // Typography styles
// #############################

exports.default = typographyStyle;

//# sourceMappingURL=styles.js.map