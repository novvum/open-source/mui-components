'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _perfectScrollbar = require('perfect-scrollbar');

var _perfectScrollbar2 = _interopRequireDefault(_perfectScrollbar);

var _reactRouterDom = require('react-router-dom');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = require('@material-ui/core/styles/withStyles');

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Drawer = require('@material-ui/core/Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _List = require('@material-ui/core/List');

var _List2 = _interopRequireDefault(_List);

var _ListItem = require('@material-ui/core/ListItem');

var _ListItem2 = _interopRequireDefault(_ListItem);

var _ListItemIcon = require('@material-ui/core/ListItemIcon');

var _ListItemIcon2 = _interopRequireDefault(_ListItemIcon);

var _ListItemText = require('@material-ui/core/ListItemText');

var _ListItemText2 = _interopRequireDefault(_ListItemText);

var _Hidden = require('@material-ui/core/Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _Collapse = require('@material-ui/core/Collapse');

var _Collapse2 = _interopRequireDefault(_Collapse);

var _HeaderLinks = require('../Header/HeaderLinks');

var _HeaderLinks2 = _interopRequireDefault(_HeaderLinks);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _avatar = require('./avatar.png');

var _avatar2 = _interopRequireDefault(_avatar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// javascript plugin used to create scrollbars on windows


// @material-ui/core components


// core components


var ps;

// We've created this component so we can have a ref to the wrapper of the links that appears in our sidebar.
// This was necessary so that we could initialize PerfectScrollbar on the links.
// There might be something with the Hidden component from material-ui, and we didn't have access to
// the links, and couldn't initialize the plugin.

var SidebarWrapper = function (_React$Component) {
	_inherits(SidebarWrapper, _React$Component);

	function SidebarWrapper() {
		_classCallCheck(this, SidebarWrapper);

		return _possibleConstructorReturn(this, (SidebarWrapper.__proto__ || Object.getPrototypeOf(SidebarWrapper)).apply(this, arguments));
	}

	_createClass(SidebarWrapper, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			if (navigator.platform.indexOf('Win') > -1) {
				ps = new _perfectScrollbar2.default(this.refs.sidebarWrapper, {
					suppressScrollX: true,
					suppressScrollY: false
				});
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (navigator.platform.indexOf('Win') > -1) {
				ps.destroy();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    className = _props.className,
			    user = _props.user,
			    headerLinks = _props.headerLinks,
			    links = _props.links;

			return _react2.default.createElement(
				'div',
				{ className: className, ref: 'sidebarWrapper' },
				user,
				headerLinks,
				links
			);
		}
	}]);

	return SidebarWrapper;
}(_react2.default.Component);

var SidebarComponent = function (_React$Component2) {
	_inherits(SidebarComponent, _React$Component2);

	function SidebarComponent(props) {
		_classCallCheck(this, SidebarComponent);

		var _this2 = _possibleConstructorReturn(this, (SidebarComponent.__proto__ || Object.getPrototypeOf(SidebarComponent)).call(this, props));

		_this2.state = {
			openAvatar: false,
			openComponents: _this2.activeRoute('/components'),
			openForms: _this2.activeRoute('/forms'),
			openTables: _this2.activeRoute('/tables'),
			openMaps: _this2.activeRoute('/maps'),
			openPages: _this2.activeRoute('-page'),
			miniActive: true
		};
		_this2.activeRoute.bind(_this2);
		return _this2;
	}
	// verifies if routeName is the one active (in browser input)


	_createClass(SidebarComponent, [{
		key: 'activeRoute',
		value: function activeRoute(routeName) {
			return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
		}
	}, {
		key: 'openCollapse',
		value: function openCollapse(collapse) {
			var st = {};
			st[collapse] = !this.state[collapse];
			this.setState(st);
		}
	}, {
		key: 'render',
		value: function render() {
			var _cx,
			    _cx2,
			    _this3 = this,
			    _cx17,
			    _cx20,
			    _cx21;

			var _props2 = this.props,
			    classes = _props2.classes,
			    color = _props2.color,
			    logo = _props2.logo,
			    image = _props2.image,
			    logoText = _props2.logoText,
			    routes = _props2.routes,
			    bgColor = _props2.bgColor,
			    rtlActive = _props2.rtlActive;

			var itemText = classes.itemText + ' ' + (0, _classnames2.default)((_cx = {}, _defineProperty(_cx, classes.itemTextMini, this.props.miniActive && this.state.miniActive), _defineProperty(_cx, classes.itemTextMiniRTL, rtlActive && this.props.miniActive && this.state.miniActive), _defineProperty(_cx, classes.itemTextRTL, rtlActive), _cx));
			var collapseItemText = classes.collapseItemText + ' ' + (0, _classnames2.default)((_cx2 = {}, _defineProperty(_cx2, classes.collapseItemTextMini, this.props.miniActive && this.state.miniActive), _defineProperty(_cx2, classes.collapseItemTextMiniRTL, rtlActive && this.props.miniActive && this.state.miniActive), _defineProperty(_cx2, classes.collapseItemTextRTL, rtlActive), _cx2));
			var userWrapperClass = classes.user + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.whiteAfter, bgColor === 'white'));
			var caret = classes.caret + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.caretRTL, rtlActive));
			var collapseItemMini = classes.collapseItemMini + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.collapseItemMiniRTL, rtlActive));
			var photo = classes.photo + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.photoRTL, rtlActive));
			var user = _react2.default.createElement(
				'div',
				{ className: userWrapperClass },
				_react2.default.createElement(
					'div',
					{ className: photo },
					_react2.default.createElement('img', { src: _avatar2.default, className: classes.avatarImg, alt: '...' })
				),
				_react2.default.createElement(
					_List2.default,
					{ className: classes.list },
					_react2.default.createElement(
						_ListItem2.default,
						{ className: classes.item + ' ' + classes.userItem },
						_react2.default.createElement(
							_reactRouterDom.NavLink,
							{
								to: '#',
								className: classes.itemLink + ' ' + classes.userCollapseButton,
								onClick: function onClick() {
									return _this3.openCollapse('openAvatar');
								}
							},
							_react2.default.createElement(_ListItemText2.default, {
								primary: rtlActive ? 'تانيا أندرو' : 'Tania Andrew',
								secondary: _react2.default.createElement('b', {
									className: caret + ' ' + classes.userCaret + ' ' + (this.state.openAvatar ? classes.caretActive : '')
								}),
								disableTypography: true,
								className: itemText + ' ' + classes.userItemText
							})
						),
						_react2.default.createElement(
							_Collapse2.default,
							{ 'in': this.state.openAvatar, unmountOnExit: true },
							_react2.default.createElement(
								_List2.default,
								{ className: classes.list + ' ' + classes.collapseList },
								_react2.default.createElement(
									_ListItem2.default,
									{ className: classes.collapseItem },
									_react2.default.createElement(
										_reactRouterDom.NavLink,
										{
											to: '#',
											className: classes.itemLink + ' ' + classes.userCollapseLinks
										},
										_react2.default.createElement(
											'span',
											{ className: collapseItemMini },
											rtlActive ? 'مع' : 'MP'
										),
										_react2.default.createElement(_ListItemText2.default, {
											primary: rtlActive ? 'ملفي' : 'My Profile',
											disableTypography: true,
											className: collapseItemText
										})
									)
								),
								_react2.default.createElement(
									_ListItem2.default,
									{ className: classes.collapseItem },
									_react2.default.createElement(
										_reactRouterDom.NavLink,
										{
											to: '#',
											className: classes.itemLink + ' ' + classes.userCollapseLinks
										},
										_react2.default.createElement(
											'span',
											{ className: collapseItemMini },
											rtlActive ? 'هوع' : 'EP'
										),
										_react2.default.createElement(_ListItemText2.default, {
											primary: rtlActive ? 'تعديل الملف الشخصي' : 'Edit Profile',
											disableTypography: true,
											className: collapseItemText
										})
									)
								),
								_react2.default.createElement(
									_ListItem2.default,
									{ className: classes.collapseItem },
									_react2.default.createElement(
										_reactRouterDom.NavLink,
										{
											to: '#',
											className: classes.itemLink + ' ' + classes.userCollapseLinks
										},
										_react2.default.createElement(
											'span',
											{ className: collapseItemMini },
											rtlActive ? 'و' : 'S'
										),
										_react2.default.createElement(_ListItemText2.default, {
											primary: rtlActive ? 'إعدادات' : 'Settings',
											disableTypography: true,
											className: collapseItemText
										})
									)
								)
							)
						)
					)
				)
			);
			var links = _react2.default.createElement(
				_List2.default,
				{ className: classes.list },
				routes.map(function (prop, key) {
					var _cx15;

					if (prop.redirect) {
						return null;
					}
					if (prop.collapse) {
						var _cx8, _cx9;

						var _navLinkClasses = classes.itemLink + ' ' + (0, _classnames2.default)(_defineProperty({}, ' ' + classes.collapseActive, _this3.activeRoute(prop.path)));
						var _itemText = classes.itemText + ' ' + (0, _classnames2.default)((_cx8 = {}, _defineProperty(_cx8, classes.itemTextMini, _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx8, classes.itemTextMiniRTL, rtlActive && _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx8, classes.itemTextRTL, rtlActive), _cx8));
						var _collapseItemText = classes.collapseItemText + ' ' + (0, _classnames2.default)((_cx9 = {}, _defineProperty(_cx9, classes.collapseItemTextMini, _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx9, classes.collapseItemTextMiniRTL, rtlActive && _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx9, classes.collapseItemTextRTL, rtlActive), _cx9));
						var _itemIcon = classes.itemIcon + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.itemIconRTL, rtlActive));
						var _caret = classes.caret + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.caretRTL, rtlActive));
						return _react2.default.createElement(
							_ListItem2.default,
							{ key: key, className: classes.item },
							_react2.default.createElement(
								_reactRouterDom.NavLink,
								{
									to: '#',
									className: _navLinkClasses,
									onClick: function onClick() {
										return _this3.openCollapse(prop.state);
									}
								},
								_react2.default.createElement(
									_ListItemIcon2.default,
									{ className: _itemIcon },
									_react2.default.createElement(prop.icon, null)
								),
								_react2.default.createElement(_ListItemText2.default, {
									primary: prop.name,
									secondary: _react2.default.createElement('b', {
										className: _caret + ' ' + (_this3.state[prop.state] ? classes.caretActive : '')
									}),
									disableTypography: true,
									className: _itemText
								})
							),
							_react2.default.createElement(
								_Collapse2.default,
								{ 'in': _this3.state[prop.state], unmountOnExit: true },
								_react2.default.createElement(
									_List2.default,
									{ className: classes.list + ' ' + classes.collapseList },
									prop.views.map(function (prop, key) {
										if (prop.redirect) {
											return null;
										}
										var navLinkClasses = classes.collapseItemLink + ' ' + (0, _classnames2.default)(_defineProperty({}, ' ' + classes[color], _this3.activeRoute(prop.path)));
										var collapseItemMini = classes.collapseItemMini + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.collapseItemMiniRTL, rtlActive));
										return _react2.default.createElement(
											_ListItem2.default,
											{ key: key, className: classes.collapseItem },
											_react2.default.createElement(
												_reactRouterDom.NavLink,
												{ to: prop.path, className: navLinkClasses },
												_react2.default.createElement(
													'span',
													{ className: collapseItemMini },
													prop.mini
												),
												_react2.default.createElement(_ListItemText2.default, {
													primary: prop.name,
													disableTypography: true,
													className: _collapseItemText
												})
											)
										);
									})
								)
							)
						);
					}
					var navLinkClasses = classes.itemLink + ' ' + (0, _classnames2.default)(_defineProperty({}, ' ' + classes[color], _this3.activeRoute(prop.path)));
					var itemText = classes.itemText + ' ' + (0, _classnames2.default)((_cx15 = {}, _defineProperty(_cx15, classes.itemTextMini, _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx15, classes.itemTextMiniRTL, rtlActive && _this3.props.miniActive && _this3.state.miniActive), _defineProperty(_cx15, classes.itemTextRTL, rtlActive), _cx15));
					var itemIcon = classes.itemIcon + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.itemIconRTL, rtlActive));
					return _react2.default.createElement(
						_ListItem2.default,
						{ key: key, className: classes.item },
						_react2.default.createElement(
							_reactRouterDom.NavLink,
							{ to: prop.path, className: navLinkClasses },
							_react2.default.createElement(
								_ListItemIcon2.default,
								{ className: itemIcon },
								_react2.default.createElement(prop.icon, null)
							),
							_react2.default.createElement(_ListItemText2.default, {
								primary: prop.name,
								disableTypography: true,
								className: itemText
							})
						)
					);
				})
			);

			var logoNormal = classes.logoNormal + ' ' + (0, _classnames2.default)((_cx17 = {}, _defineProperty(_cx17, classes.logoNormalSidebarMini, this.props.miniActive && this.state.miniActive), _defineProperty(_cx17, classes.logoNormalSidebarMiniRTL, rtlActive && this.props.miniActive && this.state.miniActive), _defineProperty(_cx17, classes.logoNormalRTL, rtlActive), _cx17));
			var logoMini = classes.logoMini + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.logoMiniRTL, rtlActive));
			var logoClasses = classes.logo + ' ' + (0, _classnames2.default)(_defineProperty({}, classes.whiteAfter, bgColor === 'white'));
			var brand = _react2.default.createElement(
				'div',
				{ className: logoClasses },
				_react2.default.createElement(
					'a',
					{ href: 'https://www.creative-tim.com', className: logoMini },
					_react2.default.createElement('img', { src: logo, alt: 'logo', className: classes.img })
				),
				_react2.default.createElement(
					'a',
					{ href: 'https://www.creative-tim.com', className: logoNormal },
					logoText
				)
			);
			var drawerPaper = classes.drawerPaper + ' ' + (0, _classnames2.default)((_cx20 = {}, _defineProperty(_cx20, classes.drawerPaperMini, this.props.miniActive && this.state.miniActive), _defineProperty(_cx20, classes.drawerPaperRTL, rtlActive), _cx20));
			var sidebarWrapper = classes.sidebarWrapper + ' ' + (0, _classnames2.default)((_cx21 = {}, _defineProperty(_cx21, classes.drawerPaperMini, this.props.miniActive && this.state.miniActive), _defineProperty(_cx21, classes.sidebarWrapperWithPerfectScrollbar, navigator.platform.indexOf('Win') > -1), _cx21));
			return _react2.default.createElement(
				'div',
				{ ref: 'mainPanel' },
				_react2.default.createElement(
					_Hidden2.default,
					{ mdUp: true },
					_react2.default.createElement(
						_Drawer2.default,
						{
							variant: 'temporary',
							anchor: rtlActive ? 'left' : 'right',
							open: this.props.open,
							classes: {
								paper: drawerPaper + ' ' + classes[bgColor + 'Background']
							},
							onClose: this.props.handleDrawerToggle,
							ModalProps: {
								keepMounted: true // Better open performance on mobile.
							}
						},
						brand,
						_react2.default.createElement(SidebarWrapper, {
							className: sidebarWrapper,
							user: user,
							headerLinks: _react2.default.createElement(_HeaderLinks2.default, { rtlActive: rtlActive }),
							links: links
						}),
						image !== undefined ? _react2.default.createElement('div', {
							className: classes.background,
							style: { backgroundImage: 'url(' + image + ')' }
						}) : null
					)
				),
				_react2.default.createElement(
					_Hidden2.default,
					{ smDown: true },
					_react2.default.createElement(
						_Drawer2.default,
						{
							onMouseOver: function onMouseOver() {
								return _this3.setState({ miniActive: false });
							},
							onMouseOut: function onMouseOut() {
								return _this3.setState({ miniActive: true });
							},
							anchor: rtlActive ? 'right' : 'left',
							variant: 'permanent',
							open: true,
							classes: {
								paper: drawerPaper + ' ' + classes[bgColor + 'Background']
							}
						},
						brand,
						_react2.default.createElement(SidebarWrapper, {
							className: sidebarWrapper,
							user: user,
							links: links
						}),
						image !== undefined ? _react2.default.createElement('div', {
							className: classes.background,
							style: { backgroundImage: 'url(' + image + ')' }
						}) : null
					)
				)
			);
		}
	}]);

	return SidebarComponent;
}(_react2.default.Component);

SidebarComponent.defaultProps = {
	bgColor: 'blue'
};

SidebarComponent.propTypes = {
	classes: _propTypes2.default.object.isRequired,
	bgColor: _propTypes2.default.oneOf(['white', 'black', 'blue']),
	rtlActive: _propTypes2.default.bool,
	color: _propTypes2.default.oneOf(['white', 'red', 'orange', 'green', 'blue', 'purple', 'rose']),
	logo: _propTypes2.default.string,
	logoText: _propTypes2.default.string,
	image: _propTypes2.default.string,
	routes: _propTypes2.default.arrayOf(_propTypes2.default.object)
};

var Sidebar = (0, _withStyles2.default)(_styles2.default)(SidebarComponent);
exports.default = Sidebar;

//# sourceMappingURL=Sidebar.js.map