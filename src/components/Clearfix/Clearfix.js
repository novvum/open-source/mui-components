import React from 'react';
import PropTypes from 'prop-types';

// mterial-ui components
import withStyles from '@material-ui/core/styles/withStyles';

const style = {
	clearfix: {
		'&:after,&:before': {
			display: 'table',
			content: '" "'
		},
		'&:after': {
			clear: 'both'
		}
	}
};

function ClearfixComponent({ ...props }) {
	const { classes } = props;
	return <div className={classes.clearfix} />;
}

ClearfixComponent.propTypes = {
	classes: PropTypes.object.isRequired
};

const Clearfix = withStyles(style)(ClearfixComponent);
export default Clearfix;
