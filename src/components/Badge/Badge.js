import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import badgeStyle from './styles';

function BadgeComponent({ ...props }) {
	const { classes, color, children } = props;
	return (
		<span className={classes.badge + ' ' + classes[color]}>{children}</span>
	);
}

BadgeComponent.propTypes = {
	classes: PropTypes.object.isRequired,
	color: PropTypes.oneOf([
		'primary',
		'warning',
		'danger',
		'success',
		'info',
		'rose',
		'gray'
	])
};

const Badge = withStyles(badgeStyle)(BadgeComponent);
export default Badge;

/**
 * @render react
 * @name Badge
 * @example
 * <center>
 * <Badge color="primary">Primary</Badge>
 * <Badge color="warning">Warning</Badge>
 * <Badge color="danger">Danger</Badge>
 * <Badge color="success">Success</Badge>
 * <Badge color="info">Info</Badge>
 * <Badge color="rose">Rose</Badge>
 * <Badge color="gray">Gray</Badge>
 * </center>
 */
