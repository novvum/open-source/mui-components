import React from 'react';
import { Card, CardHeader, CardFooter } from '../Card';
import statsCardStyle from './styles';
import { withStyles } from '@material-ui/core';

function Component(props) {
	return (
		<Card>
			<CardHeader color={props.color} stats icon>
				<CardIcon color={props.color}>{props.headerIcon}</CardIcon>
				<p className={styles.cardCategory}>{props.category}</p>
				<h3 className={styles.cardTitle}>{props.title}</h3>
			</CardHeader>
			<CardFooter stats>
				<div className={styles.stats}>
					{props.statIcon && props.statIcon}
					{props.stat}
				</div>
			</CardFooter>
		</Card>
	);
}

const StatsCard = withStyles(statsCardStyle)(Component);
export default StatsCard;

/**
 * @render react
 * @name StatsCard
 * @example
 * <div style={{padding: '25px'}}>
 * 	<StatsCard
 *    color="success"
 *    headerIcon={<h1>icon</h2>}
 *    category="Category"
 *    title="Title"
 *    statIcon={<p>stat</p>}
 *    stat="Stat"
 *  />
 * </div>
 */
