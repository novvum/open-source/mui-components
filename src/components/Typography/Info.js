import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function InfoText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.infoText}>
			{children}
		</div>
	);
}

InfoText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Info = withStyles(typographyStyle)(InfoText);
export default Info;
