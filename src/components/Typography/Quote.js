import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function QuoteText({ ...props }) {
	const { classes, text, author } = props;
	return (
		<blockquote className={classes.defaultFontStyle + ' ' + classes.quote}>
			<p className={classes.quoteText}>{text}</p>
			<small className={classes.quoteAuthor}>{author}</small>
		</blockquote>
	);
}

QuoteText.propTypes = {
	classes: PropTypes.object.isRequired,
	text: PropTypes.node,
	author: PropTypes.node
};

const Quote = withStyles(typographyStyle)(QuoteText);
export default Quote;
