import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function PrimaryText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.primaryText}>
			{children}
		</div>
	);
}

PrimaryText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Primary = withStyles(typographyStyle)(PrimaryText);
export default Primary;
