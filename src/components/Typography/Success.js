import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function SuccessText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.successText}>
			{children}
		</div>
	);
}

SuccessText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Success = withStyles(typographyStyle)(SuccessText);
export default Success;
