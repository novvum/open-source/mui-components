import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function MutedText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.mutedText}>
			{children}
		</div>
	);
}

MutedText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Muted = withStyles(typographyStyle)(MutedText);
export default Muted;
