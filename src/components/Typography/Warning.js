import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function WarningText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.warningText}>
			{children}
		</div>
	);
}

WarningText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Warning = withStyles(typographyStyle)(WarningText);
export default Warning;
