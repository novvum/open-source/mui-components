import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import typographyStyle from './styles';

function DangerText({ ...props }) {
	const { classes, children } = props;
	return (
		<div className={classes.defaultFontStyle + ' ' + classes.dangerText}>
			{children}
		</div>
	);
}

DangerText.propTypes = {
	classes: PropTypes.object.isRequired
};

const Danger = withStyles(typographyStyle)(DangerText);
export default Danger;
