import React from 'react';
// nodejs library that concatenates classes
import classNames from 'classnames';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// @material-ui/icons

// core components
import cardTextStyle from './styles/cardTextStyle';

function CardTextComponent({ ...props }) {
	const { classes, className, children, color, ...rest } = props;
	const cardTextClasses = classNames({
		[classes.cardText]: true,
		[classes[color + 'CardHeader']]: color,
		[className]: className !== undefined
	});
	return (
		<div className={cardTextClasses} {...rest}>
			{children}
		</div>
	);
}

CardTextComponent.propTypes = {
	classes: PropTypes.object.isRequired,
	className: PropTypes.string,
	color: PropTypes.oneOf([
		'warning',
		'success',
		'danger',
		'info',
		'primary',
		'rose'
	])
};

const CardText = withStyles(cardTextStyle)(CardTextComponent);
export default CardText;
