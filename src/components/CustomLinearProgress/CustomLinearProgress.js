import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import LinearProgress from '@material-ui/core/LinearProgress';

import customLinearProgressStyle from './styles';

function CustomLinearProgressComponent({ ...props }) {
	const { classes, color, ...rest } = props;
	return (
		<LinearProgress
			{...rest}
			classes={{
				root: classes.root + ' ' + classes[color + 'Background'],
				bar: classes.bar + ' ' + classes[color]
			}}
		/>
	);
}

CustomLinearProgressComponent.defaultProps = {
	color: 'gray'
};

CustomLinearProgressComponent.propTypes = {
	classes: PropTypes.object.isRequired,
	color: PropTypes.oneOf([
		'primary',
		'warning',
		'danger',
		'success',
		'info',
		'rose',
		'gray'
	])
};

const CustomLinearProgress = withStyles(customLinearProgressStyle)(
	CustomLinearProgressComponent
);
export default CustomLinearProgress;
