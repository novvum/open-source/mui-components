// @material-ui/icons
import React from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import ExpandMore from '@material-ui/icons/ExpandMore';

import accordionStyle from './styles';

class AccordionComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active: props.active
		};
	}
	handleChange = (panel) => (event, expanded) => {
		this.setState({
			active: expanded ? panel : -1
		});
	};
	render() {
		const { classes, collapses } = this.props;
		return (
			<div className={classes.root}>
				{collapses.map((prop, key) => {
					return (
						<ExpansionPanel
							expanded={this.state.active === key}
							onChange={this.handleChange(key)}
							key={key}
							classes={{
								root: classes.expansionPanel,
								expanded: classes.expansionPanelExpanded
							}}
						>
							<ExpansionPanelSummary
								expandIcon={<ExpandMore />}
								classes={{
									root: classes.expansionPanelSummary,
									expanded: classes.expansionPanelSummaryExpaned,
									content: classes.expansionPanelSummaryContent,
									expandIcon: classes.expansionPanelSummaryExpandIcon
								}}
							>
								<h4 className={classes.title}>{prop.title}</h4>
							</ExpansionPanelSummary>
							<ExpansionPanelDetails className={classes.expansionPanelDetails}>
								{prop.content}
							</ExpansionPanelDetails>
						</ExpansionPanel>
					);
				})}
			</div>
		);
	}
}

AccordionComponent.defaultProps = {
	active: -1
};

AccordionComponent.propTypes = {
	classes: PropTypes.object.isRequired,
	// index of the default active collapse
	active: PropTypes.number,
	collapses: PropTypes.arrayOf(
		PropTypes.shape({
			title: PropTypes.string,
			content: PropTypes.node
		})
	).isRequired
};

const Accordion = withStyles(accordionStyle)(AccordionComponent);

export default Accordion;

/**
 * @render react
 * @name Accordion
 * @example
 * <Accordion
 * 	active={0}
 * 	collapses={[
 * 		{
 * 			title: "SPIDER-MAN",
 * 			content:
 * 				"Bitten by a radioactive spider, Peter Parker’s arachnid abilities give him amazing powers he uses to help others, while his personal life continues to offer plenty of obstacles."
 * 		},
 * 		{
 * 			title: "BLACK PANTHER",
 * 			content:
 * 				"T’Challa is the king of the secretive and highly advanced African nation of Wakanda as well as the powerful warrior known as the Black Panther."
 * 		},
 * 		{
 * 			title: "IRON MAN",
 * 			content:
 * 				"Genius. Billionaire. Playboy. Philanthropist. Tony Stark's confidence is only matched by his high-flying abilities as the hero called Iron Man."
 * 		}
 * 	]}
 * />
 */
